<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.polytech-grenoble.fr%2Fuas%2Fpolytech%2FLOGO%2Flogo-polytech.png&f=1&nofb=1">
<img src="https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble+INP+-+%C3%89tablissement+-+Logo+-+2020+%28couleur%2C+RVB%29.png">

# Rapport de Projet : Projet Saint-Bernard

## Application de recherche de victimes en montagne à l'aide de la technologie de transmission sans-fil LoRa

### Alexandra CHATON - Thomas FRION - Romain PASDELOUP

### Filière INFO - Année 5 - Semestre 10 - 2021

### Tuteur Pédagogique : Bernard TOURANCHEAU, Réalisée en partenariat avec le PGHM 38 et avec l'aide d'Ulysse COUTAUD

![](images/dog.png)
*🐾" On garde une patte sur vous ! " 🐾*

*Ce projet fait suite au projet LoRa-valanche de 2020*

Sommaire
========

**[Sommaire](#sommaire) 1**

**[Projet Saint-Bernard](#projet-saint-bernard) 2**

> [Présentation du PGHM](#présentation-du-pghm) 2
>
> [Présentation du sujet](#présentation-du-sujet) 2
>
> [Présentation de l'équipe](#présentation-de-léquipe) 3

**[Organisation du projet](#organisation-du-projet) 3**

> [Gestion de projet](#gestion-de-projet) 3
>
> [Méthode](#méthode) 3
>
> [Planning prévisionnel](#planning-effectif) 4
>
> [Planning effectif](#planning-effectif) 4

**[Architecture technique et technologies](#architecture-technique-et-technologies) 4**

> [Architecture](#architecture) 4
>
> [Technologies](#technologies) 5
>
> [Outil de CI/CD](#outil-de-cicd) 5

**[Partie application Mobile](#partie-application-mobile) 6**

> [Conception](#conception) 6
>
> [Fonctionnalités](#fonctionnalités) 6
>
> [Difficultés rencontrées](#difficultés-rencontrées) 7

**[Partie Système Embarqué](#partie-système-embarqué) 8**

> [Réécriture du code existant](#réécriture-du-code-existant) 8
>
> [Fonctionnalités ajoutées](#fonctionnalités-ajoutées) 9
>
> [Présentation du paquet Saint-Bernard](#présentation-du-paquet-saint-bernard) 9
>
> [Difficultés rencontrées](#difficultés-rencontrées-1) 10

**[Métriques logicielles](#métriques-logicielles) 11**

> [Lignes de code et commits](#lignes-de-code-et-commits) 11
>
> [Langages](#langages) 12
>
> [Application Mobile](#système-embarqué) 12
>
> [Système Embarqué](#système-embarqué) 12
>
> [Temps ingénieur](#temps-ingénieur) 12

**[Conclusion](#conclusion) 13**

**[Glossaire](#glossaire) 14**

**[Bibliographie](#bibliographie) 14**

> [Partie Application Mobile](#partie-application-mobile-1) 14
>
> [Partie Système Embarqué](#partie-système-embarqué-1) 15

Projet Saint-Bernard
====================

Présentation du PGHM
--------------------

Le P.G.H.M, Peloton de Gendarmerie de Haute Montagne, a été créé en 1961 à Grenoble. Implanté depuis 1999 sur l'aérodrome du Versoud, sa proximité avec la base hélicoptère de la sécurité civile lui permet une grande réactivité pour les plus de 400 interventions de secours qu'il réalise chaque année dans tous les massifs de l'Isère. 

Cette intégration au milieu alpin lui permet également d'assurer la continuité des missions de la gendarmerie dans des zones difficiles d'accès. Les avalanches sont notamment un risque bien connu des 26 militaires du P.G.H.M. Dans cette situation d'urgence, la rapidité d'intervention est primordiale pour retrouver une victime ensevelie sous la neige.

Présentation du sujet
---------------------

En France, chaque année nous recensons en moyenne 15000 demandes de secours en montagne. En Isère, le nombre de secours en montagne annuel est de 400 en moyenne. Le secours en montagne est réparti entre plusieurs services et associations : Peloton de Gendarmerie de Haute Montagne (PGHM), CRS Alpes, le Groupement de Montagne des Sapeurs Pompiers (GMSP), l'Association Nationale des Médecins et Sauveteurs en Montagne (ANMSM) et le SAMU.

La plus grande difficulté dans le secours en montagne est de localiser la victime. Parfois, le fait même de savoir qu'une personne nécessite d'être secourue est une véritable problématique, notamment dans les zones blanches.

Notre projet Saint-Bernard (anciennement LoRa-valanche) essaie de répondre aux deux problématiques énoncées précédemment. Initialement, nous nous concentrions sur les secours suite à une avalanche. Après échange avec les principaux intéressés (le PGHM 38), nous avons pris la décision d'élargir l'utilisation du projet. C'est-à-dire que Saint-Bernard serait utilisé pour de la recherche de victimes tant en hiver qu'en été. Il serait ainsi utilisé comme complément au Détecteur de Victimes d'Avalanche (DVA) dans le contexte du risque d'avalanche. Mais de manière générale, il serait un outil supplémentaire à la disposition des secouristes et du grand public.

Comme nous venons de le voir : le projet Saint-Bernard a pour but premier de fournir un outil supplémentaire pour localiser les victimes en montagne. Mais, le projet a aussi pour objectif de fournir aux secouristes un outil complémentaire de traçage. En effet, pour assurer la sécurité des secouristes ces derniers ont à leur disposition l'outil Traccar. Cet outil permet d'envoyer, à une fréquence préalablement définie, les coordonnées GPS de l'utilisateur sur un serveur distant. La transmission se faisant grâce au réseau mobile (3g & 4g). La limite de cette solution est bien sûr les zones blanches en montagne.

Présentation de l'équipe
------------------------

Notre équipe est composée de trois étudiants en filière Informatique à Polytech Grenoble. Thomas FRION est chef de projet et a été responsable principalement du codage du système embarqué. Romain PASDELOUP est Git Master, il a travaillé en majeure partie sur l'application mobile et a servi de reviewer pour le code du système embarqué. Enfin, Alexandra CHATON est Scrum Master et a travaillé sur l'application mobile. Le projet a été réalisé sous la tutelle de Bernard TOURANCHEAU.

Notre équipe a travaillé en partenariat avec Olivier FAVRE du PGHM 38. Il a été notre interlocuteur pour comprendre les besoins des secouristes par rapport à notre projet. C'est donc grâce à lui que nous avons pu savoir qu'il existait déjà un prototype du projet initial LoRa-valanche. Dans cette optique, nous avons dû élargir le champ d'action de notre système et revoir son utilité.

Afin de mener à bien notre projet nous avons également fait appel à Ulysse COUTAUD. Après avoir échangé avec M. COUTAUD, ce dernier nous a conseillé, à la suite de son travail de recherche, de modifier le paramètre du CODING RATE. En effet, le paramètre de du coding rate peut influencer la portée des ondes LoRa. Le coding rate, poussé au maximum, peut faire gagner un spreading factor (+1SF).

Organisation du projet
======================

Gestion de projet
-----------------

### Méthode

Notre projet étant mené par trois personnes et avec deux sous-projets bien distincts, sa gestion fut relativement simple. La gestion se faisait via des issues sur Gitlab et une communication via discord ou messenger lorsque l'équipe ne pouvait se réunir en présentiel. Nous avons essayé de privilégier au maximum ce dernier pour pouvoir communiquer au mieux et garder notre motivation.

Notre grand challenge était de ne pouvoir s'appuyer sur une bonne documentation et une bonne organisation du git de la part du groupe précédent c'est pourquoi nous avons veillé à bien documenter et organiser notre travail.

### Planning prévisionnel

![](images/planning_prev.png)

### Planning effectif

![](images/planning_effectif.png)

En gris : ce qui n'a pas été réalisé.

Architecture technique et technologies
======================================

Architecture
------------

Du point de vue technique, le projet consiste à utiliser les capacités GPS et bluetooth des smartphones. L'idée étant d'envoyer via le bluetooth les coordonnées GPS à un boîtier équipé d'un module de communication longue distance. Dans notre cas, la technologie utilisée pour la communication longue distance est LoRa.

Le choix de l'utilisation d'un boîtier externe et du bluetooth, pour la communication entre ledit boîtier et le smartphone de l'utilisateur, se justifie par le fait, qu'à l'heure où est écrit le présent rapport, la majorité des smartphones ne sont pas équipés de capacité de communication LoRa.

Le boîtier embarqué aurait deux modes de fonctionnement : Nominal et Secours. Le mode Nominal envoie régulièrement les coordonnées GPS via la modulation LoRa, le tout en respectant la réglementation européenne sur le temps d'émission. Le mode Secours envoie plus souvent les coordonnées GPS et un beacon, mais cette fois-ci la réglementation est volontairement ignorée du fait de l'aspect secours du projet.

Technologies
------------

Pour ce projet, nous avons utilisé des technologies qui, du fait de la nature du projet et de l'héritage du projet de l'an dernier, se sont imposées d'elles-mêmes :

-   LoRa : pour la longue portée de la communication à faible coût énergétique.

-   Arduino (C/C++)

-   GPS

-   Bluetooth

Mais les autres technologies avec lesquelles nous avons travaillé ont été des choix techniques :

-   QR Code : pour simplifier l'enregistrement des cardes (LoRa) dans l'application.

-   React-Native : pour que le système d'exploitation (IOS ou Android) et sa version ne soient pas un obstacle à l'utilisation de notre application.

-   GitLab : car l'équipe de l'an dernier a utilisé GitLab pour initier le projet et parce que GitLab est gratuit, open source, l'offre gratuite est plus complète que celle de GitHub, etc.

Outil de CI/CD
--------------

Nous avons utilisé les outils fournis par GitLab pour mettre en place la génération et le déploiement de la documentation technique. La pipeline générant et déployant la documentation a été mise en place pour le code du système embarqué et le package Bluetooth Serial pour React Native (fork de Romain PASDELOUP).

De plus, une pipeline a été mise en place pour le déploiement continu de l'application Android (uniquement). Celle-ci se déclenche lors d'un événement sur master et stocke l'apk produit dans les artéfacts du repo.

 

Partie application Mobile
=========================

Conception
----------

Nous avons choisi de suivre la même architecture que l'an dernier avec les mêmes cas d'utilisation et le même modèle de tâches. Toutefois nous avons retravaillé l'IHM que nous trouvions trop simple.

![](images/ihm_abstraite_projet_Lora.png)

Grâce à cette IHM nous avons réalisé les maquettes de notre application, elles sont disponible au lien suivant :

[https://gitlab.com/info5-lora-avalanche/documentation/-/tree/master/UI\_Models](https://gitlab.com/info5-lora-avalanche/documentation/-/tree/master/UI_Models)

Fonctionnalités
-------------------------------------------------------------------------------------------------
Les QR Codes abritent les données sous ce format : 
  ---------------------------------
  {\
  \"MAC\":\"98:D3:51:FD:A2:C6\",\
  \"code\":\"1234\"\
  }

  ---------------------------------

Notre application possède les fonctionnalités minimales nécessaires à son utilisation. Ainsi il est possible d'enregistrer un appareil équipé du module Bluetooth grâce à un QR Code contenant son adresse MAC ainsi que le pin Bluetooth. Il est aussi possible d'enregistrer des appareils dites "amies" par le même procédé de scanner.

Saint-Bernard possède un écran dédié à la localisation sur une carte. En utilisant le service Openstreetmap, la position de l'utilisateur et de ses "amis" est visible grâce à des marqueurs. Ceux-ci se mettent à jour en fonction des coordonnées reçues par Bluetooth.

La communication Bluetooth fonctionne en envoi et en réception. De façon périodique l'application va transmettre au module bluetooth les coordonnées actuelles du smartphone pour qu'il puisse les faire circuler aux autres utilisateurs de Saint-Bernard via LoRa. De façon réciproque, elle reçoit ainsi les informations des autres utilisateurs de Saint-Bernard.

Si un "ami" se trouve en situation de danger (actuellement déclenché si le signal reçu est inférieur à -10), une notification s'affiche sur la page d'accueil pour l'indiquer à l'utilisateur.

Difficultés rencontrées
-----------------------

Nous avons commencé à programmer l'application en utilisant Expo. Il s'agit d'une plateforme de développement permettant de réaliser des applications React Native en programmant exclusivement en Typescript/Javascript (sans toucher à du code natif Java/Kotlin ou Objective-C). Toutefois, la plateforme étant toujours en développement, certaines fonctionnalités ne sont pas encore supportées par celle-ci (le bluetooth par exemple. Nous avons donc dû à un moment, \"éjecter\" Expo, pour pouvoir passer sur un projet React Native "nu". De ce fait, certaines fonctionnalités n'étaient plus présentes et il a fallu s'habituer à l'autre format de projet.

La gestion du Bluetooth nous a également posé problème. Comme dit précédemment, Expo ne supporte pas encore cette fonctionnalité et assez peu d'options en React Native sont disponibles. Notre solution a été de fork une bibliothèque existante reposant sur Bluetooth Serial écrit pour PhoneGap/Cordova et d'y inclure les options dont nous avions besoin (l'appairage automatique sans saisie de la part de l\'utilisateur notamment).

L'affichage de la carte a aussi été un point délicat pour le projet. En effet, la bibliothèque principale proposant de faire cela, React Native Maps, repose sur Google Maps et nécessite une clé API pour fonctionner. Notre solution a été d'utiliser un fork de cette bibliothèque proposant OSMDroid à la place de Google Maps. Toutefois, cette bibliothèque ne propose pas toutes les fonctionnalités de l'initiale.

Une dernière difficulté qui avait également été soulevée par le groupe de l'année précédente est l'exécution de fonctions en arrière-plan. En effet, il faudrait que l'envoi-réception de messages Bluetooth se fasse même si l'application n'est pas au premier plan (si l'utilisateur met son téléphone dans sa poche en skiant par exemple).

Nous avons fait des recherches à ce sujet et il existe des bibliothèques permettant apparemment de régler le souci. Cependant, par manque de temps nous n'avons pas pu essayer de mettre en place cette fonctionnalité.

De plus, n'ayant pas la possibilité de tester notre application sur un device avec IOS nous n'avons pas terminé la configuration correspondante, préférant avancer sur d'autres points en priorité.

Partie Système Embarqué
=======================

Réécriture du code existant 
---------------------------

Pour la partie embarquée (carte Sodaq Explorer), nous avons repris ce qui avait été fait l'année dernière (2020) afin de comprendre le workflow. Nous avons réalisé cette analyse, car nous souhaitions refactoriser le code de la carte et ajouter de nouvelles fonctionnalités.

Pour la refactorisation, nous avons essayé d'exploiter au maximum les capacités matériel et les fonctions fournies par la bibliothèque Arduino. Par exemple, dans la version de l'année dernière, la détection de la connexion bluetooth se faisait par le code. Le code écrit attendait la réception d'un message bluetooth et comparait le code secret (transmis en clair). Dans notre version, nous utilisons le pin State du module bluetooth. Ce pin envoie un signal lorsque la connexion est établie. Ainsi, nous avons modifié le workflow pour que la carte attende qu'une connexion bluetooth soit établie pour continuer la configuration de la carte.

Un autre exemple de refactorisation : pour la conversion d'une chaîne de caractères en hexadécimal, le code de l'an passé utilisait les pointeurs, des chaînes de caractères, comme des tableaux (sans faire d'arithmétique de pointeur), ainsi le code faisait une dizaine de lignes de code. Dans notre version, l'utilisation de l'arithmétique de pointeur permet de réduire la même fonction à 2 lignes de code.

En utilisant cette même méthode, nous avons pu réduire significativement plusieurs fonctions.

Un dernier exemple de refactorisation est la réorganisation du code : plus de fonctions (fonctions d'initialisation, fonctions de lecture des ports séries, etc.), déplacement de fonctions et constantes dans d'autres fichiers.

Fonctionnalités ajoutées
------------------------

Notre travail sur la partie embarquée ne se limite pas seulement à de la refactorisation de code. Nous avons également ajouté quelques fonctionnalités et les bases pour d'autres.

-   Lors de la configuration de la carte, nous modifions légèrement la configuration du module bluetooth. Cette modification consiste à changer le nom du module, celui-ci est renommé en "Saint-Bernard". Permettant ainsi de l'identifier facilement.

-   Utilisation des leds de la carte Sodaq pour indiquer son statut :

    -   Rouge clignotant lentement signifie que la carte est en attente d'une connexion bluetooth

    -   Bleu allumé en permanence signifie que la connexion bluetooth est établie

    -   Un clignotement bleu signifie qu'un message bluetooth (venant de l'application mobile) a été reçu

    -   Un clignotement vert signifie qu'un message LoRa contenant un paquet St-Bernard a été reçu

### Présentation du paquet Saint-Bernard

Dans les fonctionnalités que nous avons apportées, il y a notre modification du paquet St-Bernard imaginé par l'équipe précédente. Cette modification consiste à supprimer les '/' qui étaient utilisés comme séparateurs et qui prenaient 4 octets. Les 4 octets ainsi gagnés sont, dans notre version, utilisés pour encoder le numéro du paquet (un simple compteur). Nous avons également retiré le SNR, car le matériel permet d'obtenir le SNR (ou RSSI) du dernier signal __**reçu**__ et le paquet St-Bernard étant envoyé via LoRa, nous n'avons pas cette donnée lors de l'envoi. Toutefois le SNR (ou RSSI) est envoyé à l'application par le bluetooth.

Ainsi nous avons

-   Le CODE sur 3 octets : permet d'identifier le paquet LoRa entrant comme paquet St-Bernard. Vaut toujours "/\*-"

-   Le MODE sur 1 octet : permet de connaître dans quel mode de fonctionnement est l'émetteur du paquet.

    -   NOMINAL(1) : envoie un paquet St-Bernard en respectant le duty cycle.

    -   RESCUE(2) : envoie un beacon sans se préoccuper du duty cycle.

-   L'adresse MAC du module bluetooth de l'émetteur (12 octets)

-   Longitude sur 8 octets

-   Latitude sur 7 octets

-   Numéro du paquet sur 4 octets

Avec ce nouveau format de paquet, nous avons donc écrit un nouveau gestionnaire de paquet St-Bernard.

Nous avons aussi implémenté la récupération de la métrique de la qualité du signal LoRa reçu. Cette fonctionnalité va d'abord essayer d'obtenir le RSSI, si ce dernier n'est pas disponible alors le SNR sera la valeur retournée. Cette dernière contient en réalité deux informations en une :

-   Le type de valeur (RSSI ou SNR). Cette information est disponible au Bit 0 de la valeur retournée. Si le bit vaut 0 alors nous avons le RSSI, le SNR sinon.

-   La valeur de la métrique.

Par rapport au projet de l'an dernier, nous avons réalisé une documentation de notre code. La documentation du code embarqué est disponible sur le [lien suivant](https://info5-lora-avalanche.gitlab.io/arduino/)

Difficultés rencontrées
-----------------------

Une grande partie du travail pour le système embarqué fut d'améliorer le code de l'année dernière. La première des difficultés fut donc la documentation technique associée au travail de nos prédécesseurs. En effet, la documentation était incomplète et de mauvaise qualité. Par exemple, il n'y avait aucun élément explicatif concernant les connexions entre la carte Sodaq et le module BLE HC-05.

La configuration préexistante du matériel fut un problème. En effet, les deux modules HC-05 avaient une configuration différente de leur UART, ce qui a eu pour conséquence de fausser la lecture des messages reçus. Nous avons donc eu besoin de reconfigurer l'un des modules. Pour cela, nous avons dû chercher des éléments de documentation concernant le module HC-05.

Une difficulté similaire à la précédente fut rencontrée : pour la configuration de la puce LoRa, nous avions cherché la documentation du firmware de ladite puce. La documentation fournie par le constructeur a été faite de façon à couvrir un maximum de versions du modèle utilisé. Ainsi, certaines commandes que nous souhaitions utiliser ne marchaient pas avec notre matériel, la difficulté fut qu'il n'y avait aucune précision de compatibilité.

Notre dernier problème rencontré, qui est toujours d'actualité, est le fait que pour avoir un message (bluetooth ou LoRA) complet sans déchets, nous avons rendu les fonctions de lecture des buffers bloquantes. Le workflow actuel nécessite de recevoir un message LoRa pour pouvoir attendre un message bluetooth et vice versa. Nous pensions faire du multithreading, mais le processeur de la carte Sodaq n'offre pas une telle possibilité. Nous avons essayé quelques bibliothèques proposées par la communauté Arduino. Hélas, aucune des solutions testées n'a abouti au résultat souhaité. Nous réfléchissons toujours au problème actuellement.

Métriques logicielles
=====================

Lignes de code et commits
-------------------------

Notre projet compte **7376** lignes de codes pour un total de **132** commits répartis dans les repo suivants :

Thomas FRION comptabilise 48 commits (36,37% des commits) et 1790 lignes de code (24,27% des lignes de code). Il a travaillé pratiquement seul sur le repo Chip code et a contribué à Documentation et à Sandbox.

Romain PASDELOUP comptabilise 51 commits (38,63% des commits) et 2383 lignes de code réparties sur Chip Code, Sandbox et Packages. Sa part de contribution sur les 1986 lignes de Mobile App est difficile à déterminer. Il a travaillé sur tous les repo du git et est le seul contributeur du repo Packages.

Alexandra CHATON comptabilise 33 commits (25% des commits) et 1217 lignes de code sur Sandbox. Sa part de contribution sur les 1986 lignes de Mobile App est difficile à déterminer. Elle a travaillé sur tous les repo sauf Packages et Chip Code. Son travail s'est beaucoup appuyé sur la création de la documentation globale du projet, non prise en compte dans les lignes de code.

Langages
--------

### Application Mobile

![](images/langage_mobileapp.png)

### 

### Système Embarqué

![](images/langage_chipcode.png)

Temps ingénieur
---------------

Nous avons eu 96 heures de créneaux de projet. A cela on peut ajouter tout le travail effectué en dehors de ces créneaux ce qui nous permet de rajouter en moyenne 48 heures par personne.

Cela nous donne donc le calcul suivant :

96 \* 3 + 48 \* 3 = 432h ~= 62j (à raison de 7hj et par personne)

Pour rappel l'équipe de l'an dernier avait effectué au total 840 heures ce qui correspond à 120 jours.

En se basant sur la page [https://stackoverflow.com/jobs/salary](https://stackoverflow.com/jobs/salary) pour calculer le salaire que cela représente pour un.e seul.e ingénieur.e à plein temps, on obtient 38000 € pour un.e développeur.euse backend. En ajoutant la charge salariale, cela nous monte à 55100€ donc 151€/j.

Soit un total de 9362€ pour le coût humain du projet de cette année. Auquel s\'ajoutent les coups de l'année dernière : 18120€.

Le coût humain total est donc de 27482€ pour ce projet.

Pour plus de détail sur le budget du projet dans sa globalité, le rapport réalisé en cours de Management des Projets Innovants est disponible sur le gitlab du projet : [https://gitlab.com/info5-lora-avalanche/documentation/-/blob/master/Reports/Repport-MPI\_CHATON\_FRION\_PASDELOUP.pdf](https://gitlab.com/info5-lora-avalanche/documentation/-/blob/master/Reports/Repport-MPI_CHATON_FRION_PASDELOUP.pdf)

Conclusion
==========

Notre projet devait s'appuyer sur le projet de l'équipe de l'an dernier et s'en servir comme base. Or, ces bases n'étant pas suffisamment solides et documentées nous avons dû recommencer de zéro l'application mobile et refactoriser tout le code du système embarqué. Nous avons donc perdu un temps précieux à comprendre ce qui avait été fait pour, au final, ne pas en tenir compte. Nous avons toutefois pris en compte les problèmes rencontrés par l'équipe précédente, résultat nous avons : une application qui fonctionne et peut se lancer sur différentes versions d'Android (nous n'avons pas pu tester sur IOS), un code qui respecte les législations en vigueur ainsi qu'une documentation solide et qui permettra la reprise facile de notre travail à l'avenir.

Toutefois, le projet est loin d'être terminé. Il manque, entre autres, la gestion des différents modes de fonctionnement de la carte (Sodaq) et la fonctionnalité de recherche radio tant du côté du système embarqué que du côté application mobile. Le temps réduit du projet et le besoin de prise en main de nouvelles technologies ont été un frein à un avancement plus conséquent.

Nous repartons cependant avec la fierté d'avoir pratiquement respecté nos objectifs. Nous nous questionnons toutefois sur la légitimité de ce projet, qui après discussion avec le PGHM 38, sem ble déjà exister sous une forme similaire.

Dans l\'ensemble, ce projet fut intéressant par ses aspects techniques et son contexte.

Glossaire
=========

**Beacon :** Battement de coeur/signal émis périodiquement.

**BLE** : Bluetooth Low Energy ou "Bluetooth à basse consommation". Standard complémentaire à la technologie Bluetooth permettant de réduire sa consommation énergétique tout en conservant un débit similaire.

**Coding Rate** : Taux de redondance servant à la correction d'erreur. Peut être configuré.

**DVA** : Détecteur de Victime d'Avalanche. Outil utilisé par les sauveteurs en montagne pour retrouver les personnes ensevelies sous la neige

**LoRa** : Acronyme de "Long Range". Méthode de modulation permettant la transmission longue portée à faible coût énergétique.

**Multithreading** : Exécution de plusieurs tâches en parallèle sur un même appareil.

**PGHM** : Peloton de Gendarmerie de Haute Montagne.

**RSSI** : Received Signal Strength Indication. Valeur indiquant l'intensité du signal reçu.

**SNR** : Signal-to-noise Ratio ou "Rapport signal-bruit". Valeur indiquant la qualité du signal en fonction du bruit présent.

**Spreading Factor (SF)** : Étalement du signal en modifiant l'encodage en bits des symboles.

**UART :** Universal Asynchronous Receiver Transmitter. Composant permettant de faire la communication entre un composant et son port série.

Bibliographie
=============

### Partie Application Mobile

Expo : [https://expo.io/](https://expo.io/)

Expo Location: [https://docs.expo.io/versions/latest/sdk/location/](https://docs.expo.io/versions/latest/sdk/location/)

TypeORM : [https://typeorm.io/](https://typeorm.io/#/)

React-native

-   React-native: [https://reactnative.dev/](https://reactnative.dev/)

-   React Navigation : [https://reactnavigation.org/](https://reactnavigation.org/)

-   React-native Paper : [https://callstack.github.io/react-native-paper/](https://callstack.github.io/react-native-paper/)

-   React-native OpenStreetMap: [https://github.com/fqborges/react-native-maps-osmdroid](https://github.com/fqborges/react-native-maps-osmdroid)

-   React-native Bluetooth (notre package): [https://gitlab.com/info5-lora-avalanche/packages/react-native-bluetooth-serial](https://gitlab.com/info5-lora-avalanche/packages/react-native-bluetooth-serial)

-   React-native QR-Code (notre package):[https://gitlab.com/info5-lora-avalanche/packages/react-native-custom-qr-codes-expo](https://gitlab.com/info5-lora-avalanche/packages/react-native-custom-qr-codes-expo)

-   React-native Background Timer: [https://github.com/ocetnik/react-native-background-timer](https://github.com/ocetnik/react-native-background-timer)

-   React-native Background Fetch: [https://github.com/transistorsoft/react-native-background-fetch](https://github.com/transistorsoft/react-native-background-fetch)

### Partie Système Embarqué

Article concernant le coding rate :

Coutaud, U.;Heusse, M.; Tourancheau, B. LoRa Channel Characterization for Flexible and HighReliability Adaptive Data Rate in Multiple GatewaysNetworks, March 1, 2021.

Utilisation de la puce RN2483 sur la carte SODAQ Explorer

-   [http://ww1.microchip.com/downloads/en/DeviceDoc/40001784B.pdf](http://ww1.microchip.com/downloads/en/DeviceDoc/40001784B.pdf)

-   [http://ww1.microchip.com/downloads/en/DeviceDoc/RN2483-LoRa-Technology-Module-Command-Reference-User-Guide-DS40001784G.pdf](http://ww1.microchip.com/downloads/en/DeviceDoc/RN2483-LoRa-Technology-Module-Command-Reference-User-Guide-DS40001784G.pdf)

-   [http://ww1.microchip.com/downloads/en/DeviceDoc/50002346C.pdf](http://ww1.microchip.com/downloads/en/DeviceDoc/50002346C.pdf)

Communication UART sur la carte SODAQ Explorer

-   [https://www.microchip.com/forums/m979676.aspx](https://www.microchip.com/forums/m979676.aspx)

-   [https://www.microchip.com/forums/m987036.aspx](https://www.microchip.com/forums/m987036.aspx)

Fonctionnement du Bluetooth avec la puce intégrée à la carte :

-   [https://systev.com/sodaq-explorer-and-bluetooth/](https://systev.com/sodaq-explorer-and-bluetooth/)

Fonctionnement d'une communication LoRa P2P entre deux cartes

-   [https://support.sodaq.com/Boards/ExpLoRer/Examples/lora\_p2p/](https://support.sodaq.com/Boards/ExpLoRer/Examples/lora_p2p/)

-   [https://www.microchip.com/forums/m947922.aspx](https://www.microchip.com/forums/m947922.aspx)

Puissance d'émission & législation ISM

-   [https://fr.wikipedia.org/wiki/DBm](https://fr.wikipedia.org/wiki/DBm)

-   [https://www.ebds.eu/applications-docs/documentation/fréquences-libres-en-france/](https://www.ebds.eu/applications-docs/documentation/fr%C3%A9quences-libres-en-france/)
