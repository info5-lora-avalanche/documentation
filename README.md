# Project Saint-Bernard : Documentation

## Project 2020
This folder contains the various documents produced by the INFO5-2020 team.

## Existing Analysis
As this project is based on an already existing project (LoRa-valanche), in order to continue our study we made an analysis of the existing project and highlighted our future development.

## Tracking Sheet
This document summarizes the different stages of the project and the team's progress through the different sprints.
