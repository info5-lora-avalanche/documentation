# Energy survey - 2021

## Constans and formulas

```
ToA = Tpreamble + Tpayload
Tpreample = (Nsymbol + 4.25) * Tsymbol
Tsymbol = 2^(SF) / BW
Tpayload = Tsymbol * (8 + max(ceil((8*PL-4*SF+28+16*CRC+20*H) / 4*(SF-2*DE))*(CR+4),0))
```

* `Nsymbol`: Number of symbols in preamble
* `Tsymbol`: Time per symbol
* `BW`: Bandwith (in Hz)
* `SF`: Spreading factor
* `PL`: Payload size (in bytes) 
* `CRC`: 1 if enabled, 0 if disabled (default: 1)
* `H`: 0 if enabled (explicit header), 1 if disabled (implicit header) (default: 0)
* `DE`: Low Data Rate Optimization, 1 if enabled (when `BW = 125` and `SF >= 11`), 0 if disabled
* `CR`: Coding rate. Authorized values: `1, 2, 3, 4`  (default: 1 for `4/5`)

>In `EU868` standard :
>* `Nsymbol = 8`
>* `BW` can be equal to 125kHz or 250kHz

>`PL` includes the LoRaWAN header (13 bytes at least)


## Constants LoRa chip (Sodaq Explorer)

To compute the transmission power (in mW), we will use the following formula : 

```
P = 1 * 10 ^ (x / 10)
```

* `P` power in mW
* `x` power in dBm

Tranciver power: 
| TX power setting | TX power dBm | TX in mA | TX in mW |
| :---:|:---:|:---:| :---: |
| -3 | -4.0 | 17.3 | 0.4   |
| -2 | -2.9 | 18.0 | 0.51  |
| -1 | -1.9 | 18.7 | 0.65  |
| 0  | -1.7 | 20.2 | 0.68  |
| 1  | -0.6 | 21.2 | 0.87  |
| 2  | 0.4  | 22.3 | 1.1   |
| 3  | 1.4  | 23.5 | 1.38  |
| 4  | 2.5  | 24.7 | 1.78  |
| 5  | 3.6  | 26.1 | 2.29  |
| 6  | 4.7  | 27.5 | 2.95  |
| 7  | 5.8  | 28.8 | 3.8   |
| 8  | 6.9  | 30.0 | 4.9   |
| 9  | 8.1  | 31.2 | 6.46  |
| 10 | 9.3  | 32.4 | 8.51  |
| 11 | 10.4 | 33.7 | 10.96 |
| 12 | 11.6 | 35.1 | 14.45 |
| 13 | 12.5 | 36.5 | 17.78 |
| 14 | 13.5 | 38.0 | 22,39 |
| 15 | 14.1 | 38.9 | 25.7  |

> We will ignore the last row of the table, because in `EU868` standard the transmission power is limited to `25mW`



Re do :



* Package size : 53 bytes
    * 13 bytes for the header
    * 40 bytes for the payload
* Sodaq explorer power : 3,6 V 
* Power sleep mode : 0.0016 mA = 0,00576 mW (0.0016 * 3,6)

## Calculation based on Spreading Factor (SF)

Values from [Airtime calculator for LoRaWAN](https://avbentem.github.io/airtime-calculator/ttn/eu868/40) :

||SF12|SF10|
|:---:|:---:|:---:|
| **Airtime** | 616,4 ms | 2,465.8​ ms |
| **Duty cycle (99%)** | 61.6​ s | 246.6​s |


Calculated values :

||SF12|SF10|
|:---:|:---:|:---:|
| **TX** | 0,045 J | 0,013 J |
| **Sleep** | 0,0014 J | 0,00036 J |