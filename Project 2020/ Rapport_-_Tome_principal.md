# SECOURS EN MONTAGNE AVEC LORA
#### Étude de la faisabilité d’une application de recherche de victimes en montagne à l’aide de la technologie de transmission sans-fil LoRa

---
### Rapport de projet de fin d’étude - Tome principal

BADAT Leya - CUAU Victor - MASSON Jérémy - WYKLAND Damien - ZARCOS Paul

Filière INFO - Année 5 - Semestre 10

Tuteur pédagogique : Bernard TOURANCHEAU  
bernard.tourancheau@univ-grenoble-alpes.fr  
+33 4 57 42 15 64  

<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.polytech-grenoble.fr%2Fuas%2Fpolytech%2FLOGO%2Flogo-polytech.png&f=1&nofb=1">
<img src="https://master-staps.univ-grenoble-alpes.fr/medias/photo/logo-nouvelle-uga_1575556860387-jpg" height="150">


---
---
## Table des matières<a name='toc'></toc>

- [Remerciements](#remerciements)
- [Introduction](#introduction)
    - [Présentation du P.G.H.M](#introPGHM)
    - [Présentation du sujet](#introsujet)
    - [Présentation de l’équipe & gestion de projet](#introteam)
    - [Définition des objectifs et besoins du projet](#introobjf)
    - [St.Bernard : on garde une patte sur vous](#introstb)
- [Développement de l’application Android](#dev)
    - [Conception](#devcon)
    - [Fonctionnement](#devfct)
    - [Points techniques](#devpt)
    - [Problèmes rencontrés](#devpb)
    - [Métriques logicielles](#devmet)
- [Difficultés rencontrées au niveau hardware](#pb)
    - [Difficultés rencontrées avec la carte SODAQ Explorer](#pbsodaq)
    - [Difficultés rencontrées avec la RN2483a](#pbrn) 
- [Axes d’amélioration](#axes)
- [Respect de la législation des bandes ISM](#law)
- [Conclusion](#conclusion)
- [Glossaire](#glossaire)
- [Bibliographie](#biblio)
    - [A propos de l’étude du comportement des ondes LoRa dans la neige](#biblioLoRa)
    - [A propos du GPS des smartphones](#bibliogps)
    - [A propos de la carte SODAQ Explorer](#bibliosodaq)
    - [A propos de l’application Android](#biblioandroid)
- [Table des figures](#figures)



## Remerciements<a name="remerciements"></a> [↑](#toc)
Nous souhaitons remercier Bernard TOURANCHEAU, notre tuteur pédagogique et enseignant-chercheur au LIG, pour nos nombreux échanges autour des besoins de ce projet, son accompagnement régulier et ses conseils.

Nous remercions également le personnel du FabLab MSTIC du campus de l’UGA, pour les nombreux prêts de matériel que nous avons effectué, les conseils techniques qui nous ont été fourni, et l’impression en 3D du prototype du boitier de notre système.

Nous souhaitons également remercier Arnaud LEGRAND, directeur de recherche au LIG et enseignant-chercheur, pour ses explications et réflexions autour de la conception de nos protocoles de tests, du calcul de nos intervalles de confiance et de l’interprétation des résultats.

Enfin, nous remercions Grégory BIEVRE, enseignant-chercheur à l’institut ISTerre de Grenoble, pour son aimable prêt de pelles à neige et de tarières à main, qui nous ont grandement facilité la tâche lors de nos mesures en montagne.


## Introduction<a name="introduction"></a> [↑](#toc)
### Présentation du PGHM<a name='introPGHM'></a> [↑](#toc)
<img src="https://cdn-s-www.ledauphine.com/images/39D1EF8B-F682-4EA2-9CB7-73782637C85F/NW_raw/les-hommes-du-pghm-de-l-isere-le-samu-et-l-helicoptere-de-la-securite-civile-ont-effectue-plusieurs-operations-de-secours-en-montagne-ce-samedi-archives-p-hoto-le-dl-gregory-yetchmeniza-1583624261.jpg" width="40%"><a name='fig1'></a>

>  Figure 1 - Hélicoptère du PGHM de l'Isère - Archives photo Le Dauphiné Libéré, Grégory YETCHMENIZA

Le P.G.H.M, Peloton de Gendarmerie de Haute Montagne, a été créé en 1961 à Grenoble. Implanté depuis 1999 sur l’aérodrome du Versoud, sa proximité avec la base hélicoptère de la sécurité civile lui permet une grande réactivité pour les plus de 400 interventions de secours qu’il réalise chaque année dans tous les massifs de l’Isère. Cette intégration au milieu alpin lui permet également d’assurer la continuité des missions de la gendarmerie dans des zones difficiles d’accès.

Les avalanches sont notamment un risque bien connu des 26 militaires du P.G.H.M. Dans cette situation d’urgence, la rapidité d’intervention est primordiale pour retrouver une victime ensevelie sous la neige.

### Présentation du sujet<a name='introsujet'></a> [↑](#toc)
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/LVS-Tracker-DTS.jpg/800px-LVS-Tracker-DTS.jpg" width="40%"><a name='fig2'></a>

> Figure 2 - DVA numérique avec afficheur LED, Wikipédia

Plus de 15000 demandes de secours en montagne sont recensées chaque année en France. C'est plus d'une quarantaine chaque jour en moyenne. Si certaines situations sont sans grande gravité, d'autres relèvent parfois de l'urgence absolue. La recherche d'une personne victime d'une avalanche et ensevelie sous la neige en est une quand on sait qu'au-delà de 35 minutes, les chances de survie tombent à seulement 34%.

Aujourd’hui, ce sont des DVA, pour Détecteurs de Victimes d’Avalanches, qui sont utilisés par les secours. Ces systèmes fonctionnent sur le principe du radar : la victime dispose d’un boitier qui émet un signal et les secours reçoivent le signal et se laissent guider par la force du signal. Mais le signal transmis ne contient rien, et c’est bien dommage ! Imaginez que l’émetteur de la victime dispose d’un GPS, et transmette les coordonnées de sa position dans le message. Il deviendrait alors possible, en un rien de temps, de se rapprocher immédiatement à quelques mètres seulement de la victime et donc de commencer les recherches dans un périmètre infiniment réduit. Il s’agirait d’un gain de temps spectaculaire !

Il existe donc un besoin de moderniser les technologies utilisées lors de la recherche de victimes d'avalanches. Le but de ce projet est d'étudier la possibilité d'utiliser la technologie de communication sans-fil LoRa, alliée au GPS et au Bluetooth, pour retrouver plus facilement et rapidement une personne ensevelie sous la neige. Il nous faudra en proposer un prototype, utilisable par les victimes d'avalanches, les services de secours, mais également de tierces personnes.

### Présentation de l’équipe & gestion de projet<a name='introteam'></a> [↑](#toc)
Sous la direction de Bernard TOURANCHEAU, l’équipe travaillant sur ce projet est constituée de 5 étudiants en filière Informatique à Polytech Grenoble. Leya BADAT en est la cheffe de projet. Avec Victor CUAU, iels se sont concentré.e.s sur la partie preuve de concept et étude du comportement des ondes dans la neige. Iels ont également travaillé sur le code de test Arduino utilisé pour effectuer les mesures. Mais c’est Damien WYKLAND, Scrum Master de l’équipe, qui a développé le code Arduino utilisé par le produit final. Enfin, du côté applicatif de notre système, c’est Jérémy MASSON et Paul ZARCOS qui ont conçu et développé l’application Android.

En raison du caractère expérimental de ce projet, les choix techniques et les directions prisent sont sujettes à de fréquents changements, dépendant notamment des résultats des mesures sur le terrain. Pour cette raison, nous avons choisi de réaliser des sprints courts d’une semaine, afin de toujours rester flexible et de réorienter sans cesse notre travail. De plus, tout au long du projet, nous avons travaillé dans la même salle afin de garder chacun informé de toutes les facettes du sujet. La communication entre nous était essentielle.

Les résultats expérimentaux étant incertains, notre projet ne se prêtait pas forcément à une planification précise du travail à réaliser. Nous avons choisi de commencer par nous documenter, et construire un premier protocole expérimental, en parallèle d’un premier jet de la conception de l’application. Le reste du déroulement du projet a été rythmé par l’analyse des résultats, d’autres mesures, et le développement de l’application s’est calqué sur nos conclusions expérimentales pour les choix techniques et les fonctionnalités.

### Définition des objectifs et besoins du projet<a name='introobjf'></a> [↑](#toc)
Ce projet comporte 2 objectifs bien définis. 

Le premier consiste à étudier le comportement des ondes LoRa dans la neige, afin de vérifier qu'utiliser cette technologie dans le contexte d'une avalanche est un bon choix. Pour se faire, il sera nécessaire de définir un protocole expérimental et d'effectuer rigoureusement des mesures en montagne, en faisant varier différents paramètres, pour comprendre comment évolue la qualité des transmissions dans ces situations inhabituelles. L’ensemble de cette étude est disponible en annexe de ce document.

Le second volet de ce projet est bien sûr le développement du système en lui-même. Il y aura une partie hardware (carte LoRa, capteur...), et une partie software (application pour smartphone), page . La partie hardware consiste à réaliser des choix techniques sur le matériel à utiliser et son fonctionnement. Notre objectif est de réaliser un prototype du système en utilisant du matériel grand public et peu couteux, afin de proposer une alternative au DVA, fonctionnant avec LoRa et le GOS, et utilisable par tous via une connexion Bluetooth avec un smartphone sur une application simple et conviviale. Celle-ci sera adaptée à la recherche DVA et proposera des notions de groupe d’amis afin de retrouver rapidement ses proches.

Par ailleurs, afin de rendre ce prototype plus réel, un boitier sera réalisé à l’aide d’une imprimante 3D. Cette partie du projet est disponible en annexe de ce rapport.

### St.Bernard : on garde une patte sur vous<a name='introstb'></a> [↑](#toc)
Notre système aura pour vocation de sauver des vies. Un tel système est critique et nous savons que chaque détail compte dans la conception du projet pour optimiser les chances de survie des victimes.

Cependant, malgré les situations graves dans lesquelles notre système sera appelé à être utilisé, nous tenions à le rendre un peu plus humain. Son nom devait devenir celui d’un ami, que l’on glisse systématiquement dans sa poche à chaque sortie en montagne !

Nous avons choisi de l’appeler *St.Bernard*, en référence bien sûr au célèbre chien de montagne, souvent dressé pour la recherche de victimes d’avalanches. Bernard nous semblait également être un prénom sympathique, connu de tous, qui saura devenir le nom familier d’un système en lequel on a confiance pour veiller sur nous.

<img src="https://air.imag.fr/images/f/f9/Logo_StBernard.png" width="30%"><a name='fig3'></a>

> Figure 3 - Logo du système St.Bernard

## Développement de l’application Android<a name='dev'></a> [↑](#toc)
### Conception<a name='devcon'></a> [↑](#toc)
Les schémas de conception UML sont disponibles en [annexes](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/%20Rapport_-_Annexes.md#sch%C3%A9mas-de-conception-uml-de-lapplication-android-).  
Afin de réaliser notre application, nous avons réalisé des users stories et nous avons ensuite réfléchi à l’ordre et au design des pages. Les user stories retenues initialement sont les suivantes :

|  | USER STORY |
| -------- | -------- |
| US_001 | En tant que skieur·euse, je veux pouvoir me connecter en Bluetooth à ma carte depuis mon smartphone, notamment à l'aide d'un QR Code |
| US_002 | En tant que skieur·euse, je veux pouvoir sauvegarder en “favoris” des cartes, qui seront celles de mon groupe de skieur·euse·s. Cette sauvegarde se fera à l’aide d’un numéro unique à chaque carte, ou plus simplement à l'aide d'un QR Code. |
| US_003 | En tant que skieur·euse, en fonctionnement normal, c’est-à-dire lorsque je skie, je veux que la position GPS de mon smartphone soit transmise en LoRa à tous les skieur·euse·s proches, 1 fois par minute. |
| US_004 | En tant que skieur·euse, je veux que ma carte reçoive les paquets LoRa qui l’entourent et sauvegarde le dernier paquet de chaque carte.|
| US_005 | En tant que skieur·euse, je veux pouvoir afficher les cartes dont je capte le signal, et les trier par force du signal (SNR).|
| US_006 | En tant que skieur·euse, si l’un·e de mes ami·e·s se retrouve enfoui·e sous la neige, je veux pouvoir passer sa carte en mode "Alerte", pour qu'elle transmette sa position GPS toutes les 5 secondes. |
| US_007 | En tant que skieur·euse, si ma carte reçoit un paquet lui demandant de passer en mode "Alerte", la position GPS de mon smartphone est transmise toutes les 5 secondes aux skieur·euse·s à proximité. |

Notre objectif était de penser l’application afin qu’elle soit très facile à utiliser. Pour cela, le nombre d’étapes à passer pour connecter sa carte et y ajouter celles de ses ami·e·s devait être réduite au maximum, en se concentrant sur l’essentiel.

Nous avons également réfléchi aux meilleures technologies à utiliser pour simplifier le fonctionnement de l’application et en optimiser l’expérience utilisateur.

Par exemple, nous devions utiliser un moyen de communication sans fil sur courte distance avec peu de données à transmettre entre la carte et le smartphone. La carte **LoRa** possédait déjà un module Bluetooth BLE, et il nous semblait au début évident de l’utiliser. Cependant nous nous sommes aperçu·e·s au début de l’implémentation que la technologie **Bluetooth 3.0** serait plus simple d’utilisation, avec de la documentation plus facilement accessible et compréhensible à notre disposition. Par conséquent, nous avons ajouté un module Bluetooth 3.0 à la carte. Pour se connecter en Bluetooth à la carte LoRa St.Bernard, nous avons décidé de proposer aux utilisateur·trice·s deux manières différentes : soit via un QR Code, soit via une liste de tous les appareils Bluetooth disponibles autour de soi.

Le **QR Code** permet d’avoir une connexion simple et rapide. La liste de tous les appareils disponibles est utile si l’on connaît déjà la carte LoRa qui nous a été attribuée, elle s’adresse aux utilisateur·trice·s habitué·e·s au système.

Afin d’assurer le transfert des données de la carte vers le smartphone à tout moment, même en arrière-plan lorsque l’utilisateur·trice garde son smartphone dans sa poche ou l’utilise pour autre chose, il nous a fallu créer un service.

Pour cela, nous avons tout d’abord réalisé une application de test afin de prendre en main les technologies que nous allions utiliser. Nous avons ainsi testé la connexion en Bluetooth, la lecture de QR Code et le fonctionnement en **background** de l’application.

En parallèle, nous avons défini puis créé les diverses pages en fonction des besoins, avec les différents boutons et widgets nécessaires. Enfin nous avons assemblé les briques techniques dans les pages de l’application, une à une.

### Fonctionnement<a name='devfct'></a> [↑](#toc)
La première page de l’application permet de se connecter à la carte LoRa de l’utilisateur·trice. Pour cela, on peut se connecter via une liste des appareils disponibles à proximité en sélectionnant celui souhaité, ou via le QR Code qui contient les informations nécessaires à la connexion. Une fois connecté, on lance alors le service qui tourne en arrière-plan et qui communique en Bluetooth avec la carte LoRa.

<img src="https://air.imag.fr/images/2/22/QRCode_Exemple_SecoursMontagneLoRa.png"><a name='fig4'></a>

> Figure 4 - Exemple de QR Code utilisé dans le cadre de l'application

Le QR Code comporte l’adresse MAC du module Bluetooth de la carte LoRa et l’éventuel mot de passe associé nécessaire pour se connecter. Les données qui le composent sont au format XML. En voici un exemple : 
```
{
“MAC”:”98:D3:51:FD:A2:C6”,
“code”:”1234”
}
```
La seconde page de l’application permet d’ajouter des ami·e·s que l’on pourra rechercher plus facilement par la suite. La notion « d’ami·e » dans le cadre de notre projet s’apparente en fait tout simplement à une liste de cartes « favorites ». On peut les ajouter via la lecture de leur QR Code également. Il est ensuite possible de les renommer afin de facilement savoir quelle carte correspond à quelle personne. On peut aussi afficher les différentes carte LoRa dont on reçoit des paquets, mais qui ne font pas nécessairement partie de nos favoris.

Sur cette seconde page, on a également accès à l’adresse MAC de notre carte, ainsi qu’à un bouton pour ouvrir Google Maps à notre position GPS actuelle.

En appuyant sur la carte d’une personne dans la liste d’ami·e·s, généralement celle d’une victime à rechercher, on accède à la dernière page de l’application. Celle-ci est donc la page de recherche, sur laquelle on voit l’adresse MAC et le nom donné à la carte que l’on recherche. On peut aussi voir ses coordonnées GPS et l’ouvrir dans Google Maps. Enfin on dispose d’une jauge qui affiche le SNR du dernier paquet reçu en LoRa de cette carte. Cette jauge peut alors être utilisée par les sauveteur·teuse·s comme un « radar » qui indique si l’on se rapproche plus ou moins de la victime.

L’application permet uniquement de communiquer avec la carte de l’utilisateur directement, en Bluetooth. Pour communiquer avec une autre carte afin de retrouver la victime, les cartes LoRa vont communiquer entre elles puis envoyer les informations à l’application. L’application sert d’interface pour visualiser les informations des cartes LoRa et pour leur envoyer des données. Les cartes LoRa, quant à elles, ne sont en fait qu’une passerelle et ne s’occupent que de transmettre tout ce qu’elles reçoivent en Bluetooth et inversement.

### Points techniques<a name='devpt'></a> [↑](#toc)
L’application a été développée sous Android Studio. Le code est hébergé sur GitLab, rendant la collaboration et la revue de code aisée pour Jérémy de Paul, membres du projet travaillant principalement sur l’application Android.

Le lecteur de QR Code est une page qui ouvre un service Android existant. Ce dernier demande l’accès à l’appareil photo, scanne le QR Code et envoie les données à l’application. On va s’en servir afin de récupérer les données nécessaires à la connexion en Bluetooth avec la carte LoRa, c’est-à-dire le code PIN et l’adresse MAC. On va aussi l’utiliser afin de rajouter des ami·e·s plus facilement à notre liste.

Pour se connecter à la carte LoRa et échanger des informations, on va utiliser le Bluetooth. C’était le moyen le plus simple de communiquer entre la carte LoRa et l’application, et l’envoi/réception de données s’implémente assez facilement.
On utilise un **système d’encodage/décodage** afin de transmettre ces informations. Cet encodage/décodage est fait sur 40 octets. Cela nous permet de transmettre :
- le code du « **type** » de paquet (3 octets) : permet de reconnaître notre paquet LoRa parmi tous ceux qui circulent dans notre environnement, et de savoir qu’il faut le traiter `/*-`
- le **mode** (1 octet) : indique si l’on doit passer la carte en mode alerte ou non (cet octet est présent pour une fonctionnalité future que nous n’avons pas eu le temps d’implémenter, voir page )
- l’**adresse MAC** (12 octets) du module Bluetooth de la carte afin d’identifier chaque carte LoRa de manière unique
- les **coordonnées GPS** (17 octets) : latitude/longitude
- le **SNR** (3 octets) : valeur entre 99 et -99 (suffisant au vu de nos mesures, qui montrent des SNR plutôt entre -20 en 20)
- des **séparateurs** (4 octets) entre les différentes informations afin de les parser facilement

<img src="https://air.imag.fr/images/a/a8/TramePaquetLoRa_SecoursMontagneLoRa.png"><a name='fig5'></a>

> Figure 5 - Trame d'un paquet LoRa entre deux cartes

Enfin, la partie de l’application qui envoie et reçoit des données tourne en arrière-plan.

### Problèmes rencontrés<a name='devpb'></a> [↑](#toc)
Les problèmes majeurs concernant l’application étaient liés à la communication entre l’application et la carte LoRa, ainsi que la communication entre les cartes LoRa.

Nous avons aussi rencontré des problèmes dus au fonctionnement de l’application en arrière-plan. En effet, il existe de nombreuses façons, très différentes en fonction des versions d’Android, de faire tourner notre application en fond. Les versions actuelles d’Android requièrent particulièrement plus de code que les versions antérieures pour réaliser une application qui tourne en arrière-plan, ce qui a complexifié l’implémentation et nous a demandé plus de temps.

### Métriques logicielles<a name='devmet'></a> [↑](#toc)
En prenant en compte uniquement la partie développement Android, seuls Paul ZARCOS et Jérémy MASSON ont travaillé sur l’application mobile réalisée en Java avec Android Studio. L’application a été réalisée avec un total de **76 commits**. Jérémy MASSON a effectué 64% des commits, Paul ZARCOS 35% et Damien WYKLAND 1% car il a réalisé le commit initial contenant le README.md mais ne s'est pas occupé de l’application mobile.

Nous avons codé **329 lignes de codes d'essai de prise en main** en Java (pour prendre en main la communication bluetooth, la création de service en arrière plan, la géolocalisation, etc) et **1462 lignes de codes en Java pour l’application** en elle même. On peut aussi rajouter **274 lignes en XML** utilisées pour placer les différents composants de l’interface utilisateur.

##### Temps ingénieur
Nous avons eu 152h de créneaux de projet sans compter la dernière semaine où il y a eu quelques bouleversements qui ne nous pas permis de travailler à plein temps. On peut donc compter en plus 16h de travail par personne pour cette dernière semaine.

Cela nous fait un total de 152 × 5 + 16 × 5 = 840h = 120j (à raison de 7h/j pour une seule personne).

En prenant la page https://stackoverflow.com/jobs/salary pour calculer le salaire que cela représente pour un.e seul.e ingénieur.e à plein temps, on obtient 38000 € pour un.e développeur.euse backend. En ajoutant la charge salariale, cela nous monte à 55100€ donc 151€/j

Soit un total de **18120€ pour le coût humain** du projet.


## Difficultés rencontrées au niveau hardware<a name='pb'></a> [↑](#toc)
### Difficultés rencontrées avec la carte SODAQ Explorer<a name='pbsodaq'></a> [↑](#toc)
Pour ce projet, nous avons dû travailler à partir de quatre cartes conçues par la société néerlandaise SODAQ, les SODAQ ExpLoRer. Il s’agit d’outils de développement et d’évaluation spécialement élaborés pour effectuer des tests sur des modules sans fil de Microchip Technology dans un cadre de recherche. Ce contexte précis d’utilisation peut expliquer le manque de documentation sur ces cartes.

La SODAQ ExpLoRer possède quatre serials hardware (USB, module Bluetooth, module LoRaWAN et deux pins TX et RX) mais ne possède pas de serial virtuel, ce qui limite grandement le nombre de modules pouvant être ajoutés à la carte.

### Difficultés rencontrées avec la RN2483a<a name='pbrn'></a> [↑](#toc)
Les cartes SODAQ ExpLoRer sur lesquelles nous avons travaillé étaient chacune équipée d’un module RN2483a développé et conçu par Microchip Technology, une société américaine dans la fabrication de semi-conducteurs.

Ce module basé sur la technologie sans-fil LoRa s’intéresse aux bandes des 433 et des 868 MHz et embarque un protocole LoRaWAN de classe A qui permet une connexion continue à n’importe quelle infrastructure réseau qui respecte la norme LoRaWAN. Il constitue le premier module LoRa à passer les tests de certifications de l’Alliance LoRa.

Pour ce projet, nous avions dans l’idée de récupérer le RSSI afin de déterminer, grâce à la force du signal, la proximité de la carte qui envoie les paquets. Il s’agit d’une méthode qui avait été utilisée par les étudiants de l’année dernière qui travaillaient sur le projet Ski Locator.

Cependant, il faut savoir que les modules RN2483a produits jusqu’en 2018 ne pouvaient fournir que le SNR et non le RSSI. En décodant le numéro d’identification de nos différents modules grâce à un appel auprès de l’antenne française de Microchip, nous avons eu confirmation des limitations des modules que nous utilisions.

<img src="https://air.imag.fr/images/b/b7/Detail_moduleRN2483a.jpg" width="40%"><a name='fig6'></a>

> Figure 6 - Détail d'un module RN2483a de Microchip sur une carte SODAQ ExpLoRer

Par exemple, sur ce module, le numéro d’identification est 172184D, que l’on décode de cette manière : 
- 17 : correspond à l’année de production (ici 2017) ;
- 21 : correspond à la semaine de production ;
- 84D : correspond au numéro du lot.

D’autre part, malgré la présence du document officiel non mis à jour (datant donc d’avant 2018), il existe très peu de documentation concernant le module. Il n’y a également pas de librairie en C, ce qui altère grandement la flexibilité du système. Le seul exemple concret de code utilisant ce module que nous avons trouvé a été écrit par Remy VAN DER POL et Joost KOUIJZER, deux anciens stagiaires de chez SODAQ.


## Axes d’amélioration<a name='axes'></a> [↑](#toc)
Concernant l’application Android, l’idéal serait d’intégrer un panneau Google Maps directement dans l’application au lieu d’y accéder via un bouton. Ainsi, on pourrait suivre la position de la victime au fur à et à mesure sans changer d’application.

Il serait également appréciable de n’intégrer non pas la carte Google Maps, mais une carte plus précise, conçue pour représenter en détails la montagne, ses reliefs et ses caractéristiques. Certaines applications de cartographie spécialisées utilisent déjà ce type de carte, comme par exemple OruxMaps.

De plus, une fonctionnalité très importante pour améliorer la précision de la recherche de victime à l’aide du « radar » SNR serait d’ajouter la possibilité de modifier l’échelle de la jauge SNR. Il serait ainsi possible d’en observer plus facilement les variations sur les derniers mètres.

Enfin, un autre point important serait de repenser le design de l’application. Actuellement, celle-ci est fonctionnelle, mais pas véritablement esthétique. C’est pourtant un point clé pour faire adhérer les utilisateur·trice·s et rendre l’expérience simple, agréable et intuitive.

Concernant l’utilisation de la technologie LoRa, nos études ont montré que les variations du SNR à une distance d’éloignement de moins de 10 mètres entres les cartes émettrice et réceptrice sont plus visibles à la puissance d’émission minimale, c’est-à-dire -2. Or, à cette faible puissance, la portée des paquets est moindre, et cela peut retarder la réception de la position GPS de la victime par les sauveteur·euse·s. Nous pensons donc qu’il faudrait pouvoir envoyer un paquet demandant la modification de la puissance d’émission de la carte de la victime. Ainsi, par défaut, la puissance serait au maximum, soit 14. On peut alors se rapprocher de la victime grâce à la position GPS reçue. Puis, pour finir les recherches précisément en utilisant le SNR comme un radar, on pourra faire passer la carte de la victime à la puissance d’émission minimale, -2, afin d’augmenter la précision de la recherche.

Enfin, nous avions réfléchi lors de la conception de l’application et de nos réflexions en début de projet, à un mode « Alerte », déclenchable à distance par les sauveteur·euse·s sur la carte de la victime, et qui permettrait d’augmenter la fréquence d’émission des paquets. Nous avions imaginé passer d’un paquet par minute à un toutes les 5 secondes. Cependant, lors de l’implémentation, nous nous sommes rendu·e·s compte que la carte SODAQ ExpLoRer met beaucoup de temps à passer du mode réception au mode émission en P2P. Cette mauvaise gestion des modes rend actuellement inutile le mode Alerte puisque de très nombreux paquets sont alors perdus. En effet, recevoir un paquet implique de la carte réceptrice soit dans le bon mode au bon moment.

Pour régler ce problème, on peut envisager de réussir à faire basculer les cartes d’un mode à l’autre plus rapidement. Une autre solution serait que, lorsque les cartes des sauveteur·euse·s détectent qu’une carte est en mode Alerte, elles cessent d’émettre. Ainsi, au fur et à mesure, seule la carte en mode Alerte émettra des paquets et toutes les autres seront en permanence à l’écoute pour n’en manquer aucun.


## Respect de la législation des bandes ISM<a name='law'></a> [↑](#toc)
Notre système utilise la fréquence 868 MHz (868,1 ; 868,3 ; 868,5). Celle-ci fait partie de la bande ISM (pour bande Industrielle, Scientifique et Médicale) en Europe, ce qui nous contraint à respecter certaines limitations pour être en règle avec la législation en vigueur.

> Un graphique permettant de présenter la bande ISM, notamment la 868MHz, et ses caractéristiques est disponible dans la version PDF de ce rapport. Malheureusement, la page sur laquelle nous l'avons récupéré n'existe plus.

Il nous est notamment imposé de ne pas émettre plus de 1% du temps par heure sur cette bande. Cela revient à n’émettre que 36 secondes par heure. Actuellement, nos cartes émettent, lorsqu’elles ne sont pas en mode Alerte, un paquet toutes les minutes. Chaque carte envoie donc 60 paquets par heure.

On sait que le débit utile en LoRa avec un Spreading Factor de 12 est de 250 bps. Sachant que les paquets échangés entre les cartes de notre système transportent une centaine d’octets, chaque paquet nécessiterait environ <img src="http://www.sciweavers.org/tex2img.php?eq=%20%5Cfrac%7B800%7D%7B250%7D%3D3%2C2s%20&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0" align="center" border="0" alt=" \frac{800}{250}=3,2s " height="33" /> pour être émis. Cela nous amène à **192 secondes d’émission par heure, ce qui est largement supérieur aux 36 secondes réglementaires**. Il faudrait que chaque paquet prenne au maximum 600 ms pour être émis, afin que la limite ne soit pas dépassée, soit un débit de 1,3 kbit/s !

**Le temps d’émission peut potentiellement être réduit en modifiant les paquets échangés pour qu’ils soient moins conséquents. Cela est une piste d’amélioration. De plus, il risque d’être nécessaire de modifier certains paramètres de l’émission afin d’obtenir un débit utile plus important pour réduire le temps d’émission.**

On peut également imaginer que le respect de la loi, dans une situation d’urgence où la carte de la victime émettrait bien plus fréquemment, est secondaire dans la mesure où cela reviendrait à réduire la fréquence d’actualisation du radar et donc à augmenter le temps de recherche de la victime, réduisant ainsi ses chances de survie.

De plus, la loi nous impose de ne pas émettre à une puissance supérieure à 25 mW. On sait qu’à la puissance d’émission maximale, soit 14, la puissance en sortie est de 13,5 dBm. D’où le calcul de la puissance <img src="http://www.sciweavers.org/tex2img.php?eq=P%20%3D%201%20mW%20%C3%97%2010%5E%7B%20%5Cfrac%7B13%2C5%7D%7B10%7D%20%7D%20%3D%2022%2C39%20mW%20%3C%2025%20mW.&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0" align="center" border="0" alt="P = 1 mW × 10^{ \frac{13,5}{10} } = 22,39 mW < 25 mW." width="318" height="25" />


## Conclusion<a name='conclusion'></a> [↑](#toc)
Ce projet comportait deux objectifs. Le premier consistait à étudier le comportement des ondes LoRa dans la neige, afin de vérifier qu'utiliser cette technologie dans le contexte d'une avalanche est un bon choix. Le second volet était le développement du système en lui-même, avec une partie hardware (carte LoRa, capteur...), et une partie software (application pour smartphone).

Notre étude nous permet de conclure que l’utilisation de la technologie LoRa dans un contexte de recherche de victime en montagne est tout à fait possible puisque les ondes traversent la neige. Leur atténuation est assez faible pour permettre, à la puissance d’émission maximale, de recevoir la position GPS de la victime sur plusieurs centaines de mètres. De plus, à la puissance d’émission minimale, l’atténuation du SNR est assez progressive pour l’utiliser telle un radar sur les derniers mètres. Nous estimons notre système de radar SNR fiable jusqu’à 2 mètres.
L’application St.Bernard que nous avons développée comporte les quatre écrans essentiels pour être fonctionnelle : la connexion Bluetooth à la carte LoRa, notamment par scan de QR Code, l’envoi des coordonnées GPS à intervalles de temps réguliers en arrière-plan et l’affichage de la jauge de SNR. Toutes les difficultés techniques ont été résolues. Il resterait à rendre l’application plus esthétique et user friendly.

Nous sommes dans l’ensemble satisfait.e.s de l’état actuel de ce projet. Les résultats de nos expérimentations ont confirmé que l’usage de LoRa est viable dans ce contexte. De plus, nous disposons d’une application fonctionnelle, pouvant être facilement reprise par un autre groupe dans le futur pour l’améliorer et lui donner un aspect plus fini et professionnel.


## Glossaire<a name='glossaire'></a> [↑](#toc)

|  |  |
| -------- | -------- |
| Alliance LoRa | Association ouverte à but non lucratif, visant à promouvoir l’interopérabilité et la standardisation des technologies LPWAN, pour favoriser le développement de l’IoT. |
| BLE | Bluetooth Low Energy, Bluetooth à basse consommation/énergie |
| DVA | Détecteur de Victimes d'Avalanches |
| LoRa | Long Range, Radio à longue portée |
| LoRaWAN | Long Range Wide-Area Network, Réseau étendu à longue portée |
| Okumura Hata | Modèle de propagation, qui permet de décrire et de prédire le comportement d’une propagation sans-fil dans un milieu. Il a pour but de mieux anticiper et optimiser les transmissions radios, et permet d’évaluer la puissance du signal reçu en un point donné.|
| RSSI | Received Signal Strength Indication, Mesure de la puissance du signal reçu |
| Service | Partie d’une application qui tourne en arrière-plan, c’est-à-dire même lorsque l’utilisateur·trice éteint l’écran son téléphone ou utilise une autre application simultanément.|
| SNR | Signal Noise Ratio, Ratio signal-bruit|


## Bibliographie <a name='biblio'></a> [↑](#toc)
### A propos de l’étude du comportement des ondes LoRa dans la neige<a name='biblioLoRa'></a> [↑](#toc)
https://www.comsol.com/paper/download/83543/ayuso_paper.pdf  
https://www.ncbi.nlm.nih.gov/pubmed/11990140  

### A propos du GPS des smartphones<a name='bibliogps'></a> [↑](#toc)
#### Calcul de la position, estimation de l’erreur
https://tel.archives-ouvertes.fr/tel-01589215/document
#### Protocole expérimental
JAN STEPANEK, MD, and DAVID W. CLAYPOOL MD, “GPS signal reception under snow cover: a pilot study establishing the potential usefulness of GPS in avalanche search and rescue operations.”, *Wilderness and Environmental Medicine*, 8, 1997, 101-104 ; Abstract disponible ici : https://www.ncbi.nlm.nih.gov/pubmed/11990140
#### Caractérisation de la couverture du ciel
http://sciencenetlinks.com/esheets/pie-in-the-sky/

### A propos de la carte SODAQ Explorer<a name='bibliosodaq'></a> [↑](#toc)
#### Utilisation de la puce RN2483 sur la carte SODAQ Explorer
http://ww1.microchip.com/downloads/en/DeviceDoc/40001784B.pdf  
http://ww1.microchip.com/downloads/en/DeviceDoc/RN2483-LoRa-Technology-Module-Command-Reference-User-Guide-DS40001784G.pdf  
http://ww1.microchip.com/downloads/en/DeviceDoc/50002346C.pdf  
#### Communication UART sur la carte SODAQ Explorer
https://www.microchip.com/forums/m979676.aspx  
https://www.microchip.com/forums/m987036.aspx  
#### Fonctionnement du Bluetooth avec la puce intégrée à la carte (test de la librairie rn487xBle) :
https://systev.com/sodaq-explorer-and-bluetooth/  
#### Fonctionnement d’une communication LoRa P2P entre deux cartes
https://support.sodaq.com/Boards/ExpLoRer/Examples/lora_p2p/  
https://www.microchip.com/forums/m947922.aspx  
#### Puissance d’émission & législation ISM
https://fr.wikipedia.org/wiki/DBm  
https://www.ebds.eu/applications-docs/documentation/fréquences-libres-en-france/  

### A propos de l’application Android<a name='biblioandroid'></a> [↑](#toc)
#### Fonctionnement du Bluetooth sous une application Android
https://developer.android.com/guide/topics/connectivity/bluetooth  
https://github.com/android/connectivity-samples/tree/master/BluetoothLeGatt/  
https://sberfini.developpez.com/tutoriaux/android/bluetooth/  
http://tvaira.free.fr/dev/android/android-4.html  
https://stackoverflow.com/questions/38055699/programmatically-pairing-with-a-ble-device-on-android-4-4  
https://www.youtube.com/watch?v=YJ0JQXcNNTA  
https://android.googlesource.com/platform/development/+/25b6aed7b2e01ce7bdc0dfa1a79eaf009ad178fe/samples/BluetoothChat/src/com/example/android/BluetoothChat/BluetoothChatService.java  
#### Fonctionnement du GPS sous une application Android
https://developer.android.com/things/sdk/drivers/location  
#### Fonctionnement d’une application en background sous une application Android
https://fabcirablog.weebly.com/blog/creating-a-never-ending-background-service-in-android  

## Table des figures <a name='figures'></a> [↑](#toc)
[Figure 1](#fig1) - Hélicoptère du PGHM de l'Isère - Archives photo Le Dauphiné Libéré, Grégory YETCHMENIZA  
[Figure 2](#fig2) - DVA numérique avec afficheur LED, Wikipédia  
[Figure 3](#fig3) – Logo du système St.Bernard  
[Figure 4](#fig4) - Exemple de QR Code utilisé dans le cadre de l'application  
[Figure 5](#fig5) - Trame d'un paquet LoRa entre deux cartes  
[Figure 6](#fig6) - Détail d'un module RN2483a de Microchip sur une carte SODAQ ExpLoRer  


---

---
## Récapitulatif [↑](#toc)

### Résumé 
Plus de 15000 demandes de secours en montagne sont recensées chaque année en France, soit près d'une quarantaine chaque jour. Si certaines situations sont sans grande gravité, d'autres relèvent de l'urgence absolue. La recherche d'une personne victime d'une avalanche et ensevelie sous la neige en est une, quand on sait qu'au-delà de 35 minutes, les chances de survie tombent à seulement 34%.  
Aujourd’hui, ce sont les DVA qui sont utilisés par les secours. Ils fonctionnent sur le principe du radar : la victime dispose d’un boîtier qui émet un signal dont la puissance oriente les secours. Mais celui-ci ne contient rien. S’il transmettait les coordonnées GPS de la victime, il deviendrait possible de s’en rapprocher immédiatement à quelques mètres seulement !  
Il existe donc un besoin de moderniser les technologies utilisées. Le but de ce projet est donc d’étudier la possibilité d'utiliser LoRa, alliée au GPS et au Bluetooth, pour retrouver plus facilement et rapidement une personne ensevelie.  
Nous étudierons le comportement des ondes LoRa dans la neige, afin de vérifier que leur utilisation est un bon choix dans ce contexte. Pour se faire, il sera nécessaire de définir un protocole expérimental de mesures en montagne afin de comprendre comment évolue la qualité des transmissions dans ces situations inhabituelles. Puis, nous développerons le système en lui-même. Il y aura une partie hardware (carte LoRa, capteur...) et une partie software (application pour smartphone).

### Summary
More than 15,000 distress calls, nearly forty a day, are recorded each year in France in the mountains. While some situations are not that serious, others represent an absolute emergency, for instance looking for an avalanche victim buried in the snow. Indeed, the chances of survival in such situation drop to only 34% beyond 35 minutes.  
Avalanche transceivers are the tools used today by emergency services. They roughly work like a radar: the victim has a box emitting a signal whose power directs the rescue team. However, such signal is empty of data. If it could send the victim's GPS coordinates, it would be possible for the rescue team to get to a more precise location quicker.  
There is therefore a need to modernize the technologies used. The aim of this project is to study the possibility of using the LoRa technology, combined with GPS and Bluetooth, to improve the searching of a snow-buried person.  
We will study the behaviour of LoRa waves in snow, in order to confirm the choice of such technology in this context. It will be necessary to define an experimental protocol to be applied in the field in order to understand how the quality of transmissions evolves in these unusual situations. We will then focus on the system itself. There will be a hardware part (LoRa card, sensor...) and a software part (smartphone application).