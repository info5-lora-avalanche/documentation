# SECOURS EN MONTAGNE AVEC LORA
#### Étude de la faisabilité d’une application de recherche de victimes en montagne à l’aide de la technologie de transmission sans-fil LoRa

---
### Rapport de projet de fin d’étude - Annexes

BADAT Leya - CUAU Victor - MASSON Jérémy - WYKLAND Damien - ZARCOS Paul

Filière INFO - Année 5 - Semestre 10

Tuteur pédagogique : Bernard TOURANCHEAU  
bernard.tourancheau@univ-grenoble-alpes.fr  
+33 4 57 42 15 64  

<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.polytech-grenoble.fr%2Fuas%2Fpolytech%2FLOGO%2Flogo-polytech.png&f=1&nofb=1">
<img src="https://master-staps.univ-grenoble-alpes.fr/medias/photo/logo-nouvelle-uga_1575556860387-jpg" height="150">

---
---


## Table des matières<a name='toc'></a>
- [Preuve de concept : étude du comportement des ondes dans la neige](#pdc)
    - [Pré-étude en ville avec les ondes LoRa](#pdcville)
        - [Choix techniques](#pdcville1)
        - [Protocole et acquisition des mesures](#pdcville2)
        - [Observation et analyse des résultats](#pdcville3)
        - [Conclusion de la pré-étude](#pdcville4)
    - [Mesures en montagne, avec les ondes LoRa](#pdcneige)
        - [Choix techniques](#pdcneige1)
        - [Code des cartes SODAQ Explorer](#pdcneige2)
        - [Protocole et acquisition des mesures](#pdcneige3)
        - [Observation et analyse des résultats](#pdcneige4)
        - [Conclusion](#pdcneige5)
    - [Mesures en montagne, avec le signal GPS](#pdcgps)
        - [Protocole et acquisition des mesures](#pdcgps1)
        - [Observations et analyse des résultats](#pdcgps2)
        - [Conclusion](#pdcgps3)
    - [Conclusion générale de la preuve de concept](#pdcconc)
- [Schémas de conception UML de l’application Android](#schema)
    - [Diagramme de contexte](#contexte)
    - [Vue logique de haut niveau](#logh)
    - [Vue logique de bas niveau](#logb)
    - [Vue physique](#phys)
    - [Vues dynamiques](#dynq)
    - [Arbres des tâches](#tree)
    - [IHM abstraite](#ihma)
    - [IHM concrète](#ihmc)
- [Conception du prototype de boitier](#proto)
- [Code des cartes SODAQ ExpLoRer pour les mesures en montagne](#code)
    - [Code de la carte émettrice](#codee)
    - [Code de la carte réceptrice](#coder)
    - [Code final de la carte St.Bernard](#codef)
- [Table des figures](#tof)


## Preuve de concept : étude du comportement des ondes dans la neige<a name='pdc'></a> [↑](#toc)
### Pré-étude en ville avec les ondes LoRa<a name='pdcville'></a> [↑](#toc)
Pour nous préparer au mieux à notre déplacement en montagne, nous souhaitons commencer par nous confronter en amont au maximum de problèmes que nous pourrions rencontrer durant nos expérimentations. Pour cela, nous allons écrire et adapter le code de test qui sera chargé sur les cartes SODAQ Explorer, puis effectuer des mesures en champ libre en ville. Nous analyserons les résultats obtenus pour vérifier que nos mesures sont cohérentes et que notre protocole de test est correct, rigoureux et reproductible en montagne.
#### Choix techniques<a name='pdcville1'></a> [↑](#toc)
Nous réaliserons les mesures à la fréquence d’émission 868,5 MHz, qui est la fréquence la plus courante en LoRa. En effet, nous savons que la bande des 868 MHz est moins encombrée que celle des 433 MHz, même s’il ne faut pas oublier que l’onde aura une portée moins bonne en 868 MHz qu’en 433 MHz en raison de la hauteur de la plage de fréquences.  

On utilisera un Spreading Factor (SF) de 12 et augmentons le ToA (Time on Air), ce qui nous permet d’augmenter la distance de réception du signal en réduisant le débit. Cependant, cet effet secondaire n’impacte en rien notre protocole de test ni les résultats de ce dernier puisque les données à transmettre en LoRa ne représentent que quelques octets.

Afin d’apprécier quantitativement la qualité du signal en fonction de la distance et de la profondeur d’enfouissement dans la neige de la carte émettrice, nous mesurerons la valeur du SNR, c’est-à-dire le rapport signal-bruit. La valeur du RSSI n’est pas fournie par le module LoRa RN2483A que nous utilisons, nous ne sommes donc pas en mesure de la relever.

Chaque mesure sera réalisée une dizaine de fois, afin d’obtenir un intervalle de confiance correct.

On effectue d'abord les mesures avec une puissance de 14, soit une puissance de sortie de 13,5 dBm, puis de -2, soit une puissance de sortie de -2,9 dBm.

#### Protocole et acquisition des mesures<a name='pdcville2'></a> [↑](#toc)
Nous avons recherché un espace en ville, proche de Polytech Grenoble, dégagé sur quelques centaines de mètres. L’avenue Benoit Frachon, entre les arrêts Maison Communale et Edouard Vaillant de la ligne D de tramway, nous a semblé être un bon choix.

Nous avons cherché à placer la carte émettrice à un endroit sans proximité immédiate avec un bâtiment. Nous avons trouvé une bouche à incendie qui nous a semblé être appropriée et sur laquelle la carte émettrice est posée à plat, à environ 1 mètre au-dessus du sol. Cette carte est reliée électriquement à une batterie externe qui est posée à côté d’elle.

La carte réceptrice est tenue à plat, dans la paume de la main de la personne effectuant les mesures. Elle est également reliée électriquement à une batterie externe, et connectée à un module Bluetooth qui transmet les mesures de la valeur du SNR en temps réel sur le smartphone de l’opérateur·trice. Les données sont ainsi relevées et sauvegardées.

La carte émettrice émet un paquet en LoRa toutes les secondes et la carte réceptrice affiche la valeur du SNR de ceux qu’elle reçoit.

L’opérateur·trice déplace la carte réceptrice, en ligne droite, tous les 20 pas, à partir de 0 pas et jusqu’à 300 pas. A chaque distance multiple de 20 pas, l’opérateur·trice relève la valeur du SNR calculée par la carte. Cette valeur est relevée une dizaine de fois de suite afin d’obtenir un bon intervalle de confiance.

<img src="https://air.imag.fr/images/e/e2/PdC_Pr%C3%A9%C3%A9tudeenville.png" width="60%"><a name='fig1&2'></a>
> Figures 1 et 2 : Pré-étude : Emplacement géographique des mesures & Pré-étude : Affichage des mesures du SNR sur smartphone via Bluetooth

#### Observation et analyse des résultats<a name='pdcville3'></a> [↑](#toc)
Avant toute chose, il est à noter que les conditions dans lesquelles ont été effectuées les mesures ne sont pas les plus adéquates : même si nous avions un champ libre (les cartes étaient toujours à vue l’une de l’autre), il ne faut pas oublier que les perturbations extérieures étaient malgré tout importantes. Nous avions, entre autres, le passage périodique de rames de tramway (les mesures étaient cependant suspendues à son passage), les allées et venues de piéton·ne·s possédant, voire utilisant, leur smartphone, la présence régulière de lampadaires et de poteaux en métal soutenant les lignes de tramway et enfin le passage constant de voitures (passage parallèle, qui ne coupait pas le chemin en ligne de vue directe de carte à carte).

Le premier graphique que nous obtenons nous permet de visualiser simultanément l’ensemble des mesures réalisées. En bleu, les mesures réalisées avec une puissance d’émission à 14 et en orange celle à -2.

L’étalement des points témoigne de l’instabilité de la valeur à laquelle on s’intéresse, le SNR, sujet aux variations du bruit de l’environnement. Ces disparités peuvent s’expliquer notamment par les conditions de l’expérience décrites ci-dessus. On constate un écart-type de 0 à 5,59 pour les mesures du SNR à une puissance d’émission de 14 et un écart-type de 0,28 à 4,76 pour les mesures du SNR à une puissance de -2. Bien sûr, le faible nombre de mesure réalisé en chaque point pour cette pré-étude, une dizaine seulement, ne nous permet de donner des résultats très fiables. Il est également à noter la perte de paquets à partir d’une certaine distance d’éloignement (environ 160 pas) dans le cas d’une émission à puissance minimum. Nous n’en avons cependant pas relevé le taux lors de cette pré-étude.

Toutefois, on devine sans difficulté que le SNR a tendance à diminuer avec la distance.

Nous constatons également que la puissance d’émission du signal influe indéniablement sur l’atténuation : une puissance d’émission plus forte rend l’atténuation moins importante, ou du moins plus tardive en termes de distance.

<img src="https://air.imag.fr/images/8/82/PdC_P%C3%A9%C3%A9tudeSNR.png"><a name='fig3'></a>
> Figure 3 - Pré-étude : Représentation graphique des valeurs du SNR en fonction de la distance

Un calcul puis un tracé de la moyenne des mesures à chaque distance nous permettent de mieux appréhender l’atténuation générale du SNR.

On observe une courbe de tendance qui ressemble à un polynôme de degré 8 dans chaque cas avec des variations sur les coefficients des polynômes. Cette atténuation particulière, avec deux rebonds très visibles sur les points à -2 de puissance en orange, ne nous paraît pas propre au phénomène physique mais plutôt à l’espace utilisé pour nos mesures. En effet, cela pourrait tout à fait correspondre aux deux croisements de la ligne direct entre nos cartes émettrices et réceptrices avec l’arc de cercle formé par les lampadaires et les poteaux de la ligne de tramway. Aussi, nous ne pensons pas obtenir un résultat aussi particulier dans le cadre des mesures en montagne.

On observe une atténuation propre et relativement stable de la valeur du SNR pour une distance inférieure à 100 pas, avec les deux puissances.

<img src="https://air.imag.fr/images/6/63/PdC_P%C3%A9%C3%A9tudeSNR2.png"><a name='fig4'></a>
> Figure 4 - Pré-étude : Représentation graphique de la moyenne des SNR en fonction de la distance

#### Conclusion de la pré-étude<a name='pdcville4'></a> [↑](#toc)
Notre protocole de test nous semble rigoureux, utilisable et reproductible en montagne. Nous sommes parvenu·e·s, malgré une mesure approximative des distances, à obtenir des tracés relativement propres. On observe qu’une émission plus puissante permet de capter plus loin, en retardant l’atténuation du signal avec la distance. Cependant, elle rend nos mesures moins sensibles à la distance d’éloignement.

Nous pensons pour le moment que notre application de recherche de victime utilisera LoRa avec la puissance 14. Cependant, il y a ici matière à réflexion : nos résultats semblent indiquer qu’il sera plus facile de retrouver une victime sous la neige à l’aide de la valeur du SNR si sa carte émet à une faible puissance. C’est à réfléchir et nos expériences en montagne devraient nous guider davantage dans ce choix.

Lors des mesures en montagne, nous utiliserons un outil de mesure plus précis afin de mesurer rigoureusement les distances entre les cartes émettrices et réceptrices.

Afin de simplifier le dénombrement des mesures successives relevées en un même point, nous allons ajouter un compteur devant la valeur du SNR affichée sur le smartphone de l’opérateur·trice. Ce compteur pourra être réinitialisé par un appui sur le bouton de la carte SODAQ ExpLoRer réceptrice.

Nous allons également ajouter l’affichage de la fréquence utilisée, du Spreading Factor et de la puissance, afin d’éviter toute erreur lors de l’acquisition des mesures.

Un relevé de 10 valeurs par configuration enfouissement/distance ne nous a pas semblé suffisant pour obtenir un bon intervalle de confiance lorsque le SNR était assez instable. Nous choisissons de réaliser 60 mesures lorsque nous serons en montagne.

### Mesures en montagne, avec les ondes LoRa<a name='pdcneige'></a> [↑](#toc)
#### Choix techniques<a name='pdcneige1'></a> [↑](#toc)
Nous réaliserons les mesures à la fréquence d’émission 868 MHz, qui est la fréquence la plus courante en LoRa.

On utilisera à nouveau un Spreading Factor (SF) de 12 et augmentons le ToA (Time on Air), afin d’augmenter la distance de réception.

Afin de gagner du temps, on enfouira simultanément deux cartes émettrices, à deux profondeurs différentes, afin d’effectuer les mesures en parallèle. Dans ce cas, on utilisera toujours un SF de 12, mais on fera varier le canal utilisé. En effet, on dispose de trois canaux (de 125 KHz de bande passante chacun) : 868,10 ; 868,30 et 868,50 MHz. On prendra une paire de cartes à 868,10 MHz et une autre à 868,50 MHz.

Afin d’apprécier quantitativement la qualité du signal en fonction de la distance et de la profondeur d’enfouissement dans la neige de la carte émettrice, nous mesurerons la valeur du SNR. La valeur du RSSI n’est pas fournie par le module LoRa RN2483A que nous utilisons, nous ne serons pas en mesure de la relever.

Chaque mesure, c’est-à-dire une configuration du couple (p,d) avec p la profondeur d’enfouissement et d la distance entre les cartes émettrices et réceptrices, sera réalisée 60 fois, afin d’obtenir un bon intervalle de confiance de Student, meilleur que celui de la pré-étude en ville.

On effectuera d'abord les mesures avec une puissance de 14, puis de -2.

La carte émettrice sera placée à la verticale, antenne dirigée vers le haut. La carte réceptrice sera placée à l’horizontale, antenne dirigée à 90° de la carte émettrice.

#### Code des cartes SODAQ Explorer<a name='pdcneige2'></a> [↑](#toc)
Un exemple du code des cartes, pour une fréquence de 868,10 MHz et à la puissance d’émission minimale (-2), est disponible pour les cartes émettrice et réceptrice, respectivement aux pages  et .

#### Protocole et acquisition des mesures<a name='pdcneige3'></a> [↑](#toc)
Nous nous sommes rendu·e·s en montagne le jeudi 20 février 2020, à Chamrousse, et avons cherché un endroit assez dégagé et avec une profondeur de neige suffisante pour effectuer nos mesures.

Les coordonnées GPS du lieu choisi sont : 45°06'17.3"N ; 5°52'15.0"E. Il est situé à 1668 mètres d’altitude. La température lors des mesures est de 8°C. L’humidité annoncée est de 50%. La pression annoncée est de 1025,5 HPa.

En cet endroit, la profondeur maximale de neige disponible était de 80 cm. Nous faisons donc le choix de réaliser nos mesures pour les profondeurs suivantes d’enfouissement de la carte émettrice dans la neige : 0 m ; 0,2 m ; 0,4 m ; 0,6 m ; 0,8 m. Au point où les cartes émettrices seront enfouies, nous mesurons une masse volumique de neige de 489 g.L-1.

Situé en bord de route à la lisière de la forêt, notre lieu de mesure nous offre un champ libre de 64 mètres pour effectuer nos mesures en ligne droite, avec un léger dénivelé. Nous faisons le choix de réaliser nos mesures aux distances suivantes : 0 m ; 0,5 m ; 1 m ; 2 m ; 4 m ; 8 m ; 16 m ; 32 m ; 64 m. En effet, cela est suffisant pour l’étude de l’évolution du SNR : comme nous l’avons constaté lors de la pré-étude en ville, les valeurs du SNR ne sont significatives qu’en dessous de 100 mètres de distance. De plus, les coordonnées GPS fournies par la carte de la victime sont précises à 5 mètres, il ne sera donc jamais nécessaire de s’orienter à une si lointaine distance de la victime à l’aide du SNR. De plus, les valeurs du SNR ne sont pas exploitables au-delà de 60 mètres.

On commence par enfouir deux cartes émettrices à deux profondeurs différentes. Puis, avec deux cartes réceptrices, on effectue à chaque distance 60 mesures du SNR. On réalise cela à chaque distance, pour chaque profondeur d’enfouissement, comme le montre la Figure 5, et pour les deux puissances d’émission.

<img src="https://air.imag.fr/images/8/83/PdC_Protocole.png" width="80%"><a name='fig5'></a>
> Figure 5 - Mesures en montagne : Schéma du protocole expérimental

<img src="https://air.imag.fr/images/a/a6/PdC_terminal.png"><a name='fig6'></a>
> Figure 6 - Mesures en montagne : Affichage du SNR sur smartphone via Bluetooth

<img src="https://air.imag.fr/images/0/0d/PdC_Chamrousse.png"><a name='fig7'></a>
> Figure 7 - Mesures en montagne : Emplacement géographique

#### Observation et analyse des résultats<a name='pdcneige4'></a> [↑](#toc)
Les graphiques Figure 8 et Figure 9 ci-dessous représentent l’évolution de la moyenne du rapport signal-bruit, en fonction de la distance d’éloignement entre les cartes émettrice et réceptrice. Chaque courbe correspond à une profondeur d’enfouissement p de la carte émettrice dans la neige. Les séries de données ci-dessous sont agrémentées de courbes de tendance polynomiales de degré 2. Chaque point est accompagné d’une marge d’erreur représentant l’intervalle de confiance pour une loi normale.

<img src="https://air.imag.fr/images/c/c5/PdC_SNR1.png"><a name='fig8'></a>
> Figure 8 - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission 14

<img src="https://air.imag.fr/images/5/56/PdC_SNR2.png"><a name='fig9'></a>
> Figure 9 - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission -2

On observe, comme attendu, que le SNR diminue avec la distance, dans tous les cas. L’ensemble de nos courbes sont décroissantes.  

De plus, comme attendu, à la puissance d’émission maximale, l’atténuation du SNR est plus faible avec la distance. A partir de 8 mètres de distance et au-delà, on observe une atténuation très lisse. Cependant, à moins de 8 mètres, les résultats obtenus semblent plus aléatoires.  

A la puissance d’émission minimale, l’atténuation du SNR est plus rapide avec la distance. En revanche, l’atténuation est lisse bien plus tôt, dès 2 mètres de distance. Il semble donc plus facile de se servir du SNR comme « radar » lorsque l’on utilise une faible puissance d’émission.

Pour les mesures à puissance maximale, on constate que les meilleures valeurs de SNR sont obtenues pour la profondeur maximale d'enfouissement et lorsque la carte est complètement à la surface. Cette observation va à l’encontre de toute intuition que nous aurions pu avoir quant aux résultats à attendre. A l’inverse, pour les mesures à puissance minimale, la profondeur d’enfouissement maximale fournit les plus basses valeurs de SNR, ce qui va dans la direction de nos intuitions, comme le modèle théorique le prédit.  

Ces résultats ne sont donc pas tous conformes au modèle de propagation standard Okumura Hata. Cela peut s’expliquer par la versatilité de la propagation sans fil et par le fait que nous ne maitrisions pas totalement toutes les conditions de l’expérience.  

Par ailleurs, durant l’acquisition des mesures du SNR, il était nécessaire de s’assurer que la carte réceptrice reste toujours dans la même position par rapport à la carte émettrice. En effet, une rotation de l’antenne de la carte réceptrice, même légère, entraîne une baisse ou une remontée importante du SNR sans qu’il n’y ait de translation au niveau de la position (distance d’éloignement identique entre les cartes émettrice et réceptrice). Cette sensibilité à l’orientation de la carte peut induire des erreurs dans nos mesures, et dégrader la qualité de nos résultats.  

D’autre part, nous pouvons supposer que ces aléas dans les mesures sont également dus à l’environnement dans lequel a été conduite l’expérience. En effet, en raison de l’enfouissement dans la neige, les ondes étudiées ont traversé un milieu de propagation non homogène, puisqu’il s’agit d’un mélange d’eau liquide, d’eau solide et d’air, ce qui diffère de celui “homogène” que nous avions lors de la pré-étude menée en champ libre en ville.  

Cependant, les intervalles de confiance affichés ne s’entrecoupent pas sur chaque courbe, donc sur chaque profondeur, pour les distances de 8 mètres et plus. On a donc un système assez précis et fiable pour se laisser guider par le « radar » SNR.

Il est nécessaire de discuter également ici d’un phénomène que nous avons observé lors de la réalisation de nos mesures en montagne, mais qui n’est pas représenté dans les graphiques ci-dessus : la perte de paquets. Celle-ci a été observée à la puissance d’émission minimale uniquement, à partir d’une distance d’éloignement de 16 mètres, et pour une profondeur d’enfouissement de la carte émettrice dans la neige de 40 cm et plus. Le taux de perte de paquet observé a été, dans le pire des cas, c’est-à-dire à 64 mètres de distance et pour une profondeur d’enfouissement de 80 cm, de 14,29%.

Intéressons-nous maintenant de plus près au comportement des ondes LoRa pour les petites distances d’éloignement entre les cartes émettrice et réceptrice. Les graphiques Figure 10 et Figure 11 présentent les mêmes résultats que précédemment, mais en se concentrant sur les distances d’éloignement inférieures à 9 mètres.

<img src="https://air.imag.fr/images/0/05/PdC_SNR3.png"><a name='fig10'></a>
> Figure 10 - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission 14 (zoom)

<img src="https://air.imag.fr/images/1/1c/PdC_SNR4.png"><a name='fig11'></a>
> Figure 11 - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission -2 (zoom)

Pour la puissance d’émission maximale, on observe que les résultats semblent assez aléatoires. Il existe des augmentations du SNR alors que l’on éloigne les cartes émettrice et réceptrice. Cela pourrait induire en erreur lors de la recherche d’une victime.  

En revanche, pour la puissance d’émission minimale, bien que la variation du SNR soit très faible sur les 4 premiers mètres, on observe une tendance décroissante constante, à l’exception d’un point pour la profondeur p=0,20 m pour d=1 m. Malgré cela, les valeurs moyennes du SNR relevées à la distance d=0 m restent les plus élevées.  

Afin de rendre les deux courbes précédentes plus lisibles, la Figure 12 présente uniquement les points pour la profondeur d’enfouissement dans la neige de la carte émettrice à 80 cm, pour la puissance d’émission minimale. On observe que le SNR est bien globalement décroissant avec la distance, même sur l’intervalle [0 ; 8], comme le montre l’interpolation linéaire ci-dessous, bien que la pente soit moins marquée que pour les points plus éloignés. 

<img src="https://air.imag.fr/images/f/f4/PdC_SNR5.png"><a name='fig12'></a>
> Figure 12 - Mesures en montagne : Moyenne du SNR selon la distance, prof 40 cm, puiss -2 (zoom)

#### Conclusion<a name='pdcneige5'></a> [↑](#toc)
Les résultats analysés précédemment présentent tous des courbes de tendance décroissantes, démontrant l’atténuation du rapport signal-bruit en fonction de l’augmentation de la distance entre les cartes émettrice et réceptrice. Cela tend à confirmer que l’usage de la valeur du SNR des paquets reçus tel un « radar », dans le but de retrouver une victime sous la neige, est possible et fiable.

De plus, nous observons que, pour la puissance d’émission minimale, la décroissance du SNR en fonction de la distance est très lisse et marquée dès 4 mètres de distance et au-delà. Cette bonne qualité de nos courbes se retrouve pour chaque profondeur d’enfouissement de la carte émettrice dans la neige testée. On peut donc dire avec confiance qu’à la puissance minimale d’émission, le SNR des paquets LoRa peut être utilisé avec fiabilité comme un « radar » pour retrouver une personne ensevelie sous la neige, à 2 mètres près.

Enfin, malgré une décroissance moins marquée des valeurs moyennes du SNR pour la puissance d’émission minimale à des distances d’éloignements inférieures à 2 mètres, les résultats restent, à notre sens, assez fiables pour retrouver une victime ensevelie sous la neige à ± 2 m dans changer d’échelle. Une fonctionnalité que nous n’avons pas eu le temps d’implémenter dans notre application serait de pouvoir modifier l’échelle de la jauge SNR afin d’en voir plus précisément les variations. On peut ainsi espérer atteindre une précision de ± 0,5 m, mais cela reste à vérifier expérimentalement.

### Mesures en montagne, avec le signal GPS<a name='pdcgps'></a> [↑](#toc)
Dans le but de compléter notre étude et parce que les coordonnées GPS du smartphone de la victime seront utilisées dans le premier temps de la recherche avant de passer en mode « radar » avec le SNR, nous souhaitions évaluer la précision du GPS de nos smartphones lorsque ceux-ci sont enfouis dans la neige. 

#### Protocole et acquisition des mesures<a name='pdcgps1'></a> [↑](#toc)
Nous nous sommes à nouveau rendu·e·s en montagne le vendredi 6 mars 2020, à Chamrousse, au même endroit que la dernière fois. Les coordonnées GPS du lieu choisi sont, pour rappel : 45°06'17.3"N ; 5°52'15.0"E. Il est situé à 1668 mètres d’altitude.

La température lors des mesures est de 3,8°C. L’humidité annoncée est de 89%.

La couverture nuageuse était de 100% au début, puis d’environ 50% au cours de l’expérience.

Nous allons enfouir un smartphone dans la neige et relever la latitude, la longitude et l’altitude calculée par son GPS. Pour cela, nous utiliserons l’application Capteurs Multimédias sous Android, qui affiche les données du GPS notamment, comme le montre la figure ci-contre. On lancera une capture d’écran vidéo afin de récupérer les données lorsque le smartphone est enfoui.

En cet endroit, la profondeur maximale de neige disponible était de 90 cm. Nous faisons donc le choix de réaliser nos mesures pour les profondeurs d’enfouissement du smartphone suivantes dans la neige : 0 m ; 0,3 m ; 0,5 m ; 0,7 m ; 0,9 m. Au point où le smartphone sera enfoui, nous mesurons une masse volumique de neige de 316 g.L-1.

<img src="https://air.imag.fr/images/b/b2/PdC_GPS1.png"><a name='fig13'></a>
> Figure 13 - Mesures en montagne : Données GPS

#### Observations et analyse des résultats<a name='pdcgps2'></a> [↑](#toc)
La carte Figure 14 présente les points GPS relevés par le smartphone aux différentes profondeurs d’enfouissement de l’appareil dans la neige. On observe que les coordonnées GPS calculées sont toutes légèrement différentes. Cependant, l’écart maximal relevé dans cette série de mesures est de 5 mètres (trait noir). On peut donc dire que jusqu’à 90 cm d’enfouissement dans la neige, dans les conditions dans lesquelles ont été réalisés nos mesures, il existe un rayon d’incertitude de 5 mètres autour des coordonnées GPS calculées par le smartphone et transmises en LoRa.

Par ailleurs, on sait que la précision d’un GPS est d’environ 3 à 5 mètres. Les décalages observés lors de nos mesures sont donc tout à fait dans la normale.

<img src="https://air.imag.fr/images/f/fb/PdC_GPS2.png" width="80%"><a name='fig14'></a>
> Figure 14 - Mesures en montagne : Relevés GPS sur carte

Afin de nuancer les écarts observés sur ces relevés, nous avons ajouté, sur la Figure 15, les points GPS relevés par le smartphone à 0 mètre d’enfouissement, mais à différents moments. Ainsi, le point violet isolé correspond toujours à la mesure à 0 mètre. Cependant, les points violets autour du point bleu correspondent aux coordonnées relevées juste avant et juste après avoir enfoui le smartphone dans la neige, donc aux moments où il était manipulé dans nos mains, à la surface. De même pour les points violets autour du point jaune. En revanche, les points rouge et vert ne présentent pas de points violets supplémentaires car les coordonnées relevées n’ont pas changé avant, pendant et après l’enfouissement dans la neige.

Ces points supplémentaires montrent qu’en fait, l’écart notable des coordonnées GPS relevées entre le moment où la carte est en surface et où elle est enfouie sous la neige est faible : de l’ordre de 2 mètres seulement. De plus, on observe que pour les plus faibles profondeurs d’enfouissement, c’est-à-dire 30 et 50 cm, l’enfouissement dans la neige ne modifie pas du tout les coordonnées GPS relevées.

<img src="https://air.imag.fr/images/9/97/PdC_GPS3.png" width="80%"><a name='fig15'></a>
> Figure 15 - Mesures en montagne : Relevés GPS sur carte + points à 0 mètres

#### Conclusion<a name='pdcgps3'></a> [↑](#toc)
Nous avons observé que les variations de la latitude et de la longitude calculées par le GPS du smartphone de la victime et transmises en LoRa entre les cartes ne sont pas un frein, malgré un enfouissement de la victime jusqu’à 90 cm dans la neige, pour permettre aux sauveteur·euse·s de se rapprocher dans un rayon de 5 mètres. De plus, la difficulté à creuser la neige a probablement induit un décalage d’enfouissement du smartphone de quelques dizaines de centimètres entre chaque profondeur d’enfouissement, ce qui peut justifier en partie les décalages observés.

On peut donc dire, avec confiance, que jusqu’à 90 cm d’enfouissement dans la neige, le GPS du smartphone de la victime transmet en Bluetooth à la carte LoRa des coordonnées GPS fiables dans un rayon de 4 à 5 mètres.

Cependant, cette imprécision de quelques mètres n’altère pas la fiabilité de notre projet. En effet, après s’être rendu·e·s au point indiqué par le GPS, les sauveteur·euse·s peuvent utiliser les paquets LoRa transmis par la carte de la victime tels un « radar », basé sur la valeur du SNR, pour parcourir les derniers mètres. Dans tous les cas, la réception d’un seul paquet LoRa de la victime contenant une position GPS permet de réduire considérablement, c’est-à-dire à quelques mètres, le périmètre de recherche, et cela en un temps record.

### Conclusion générale de la preuve de concept<a name='pdcconc'></a> [↑](#toc)
Rappelons que le but de ces expérimentations était de démontrer que l’utilisation des ondes LoRa dans le cadre de la recherche de victimes ensevelies sous la neige est un choix viable.

Nous avons montré que le SNR des paquets LoRa reçus diminue avec la distance d’éloignement entre les cartes émettrice et réceptrice. Il est alors possible de suivre cette valeur du SNR pour être guidé·e vers une victime ensevelie sous la neige.

Nous avons observé que l’atténuation du SNR est assez marquée pour permettre de l’utiliser comme un radar et retrouver une victime à ± 2 m. Cette précision pourra probablement être améliorée en implémentant une fonctionnalité de modification de l’échelle de la jauge SNR sur l’application.

De plus, nous avons montré que les coordonnées GPS calculées par le smartphone ne sont pas significativement erronées lorsque ce dernier est enfoui dans la neige. Ainsi, il est tout à fait fiable de se rendre sur le lieu indiqué par le GPS afin de retrouver la victime, avant de finir de se rapprocher en utilisant le SNR comme un radar. Le GPS est fiable dans un rayon de 4 à 5 mètres, réduisant ainsi drastiquement et très rapidement le périmètre de recherche.

Ainsi, nous pouvons conclure que l’utilisation de la technologie LoRa dans un contexte de recherche de victime en montagne est tout à fait possible puisque les ondes traversent la neige. Leur atténuation est assez faible pour permettre, à la puissance d’émission maximale, de recevoir la position GPS de la victime sur plusieurs centaines de mètres sans aucun problème. De plus, à la puissance d’émission minimale, l’atténuation du SNR est assez progressive pour l’utiliser telle un radar sur les derniers mètres. Nous estimons notre système de radar SNR fiable jusqu’à 2 mètres. Pour les 2 derniers mètres, cela nécessiterait une étude plus approfondie.


## Schémas de conception UML de l’application Android<a name='schema'></a> [↑](#toc)
### Diagramme de contexte<a name='contexte'></a> [↑](#toc)

<img src="https://air.imag.fr/images/6/66/DiagContextePGHM.png" width="80%"><a name='fig16'></a>
> Figure 16 - Application Android : Conception - Diagramme de contexte

### Vue logique de haut niveau<a name='logh'></a> [↑](#toc)

<img src="https://air.imag.fr/images/a/ad/VueLogique.png" width="80%"><a name='fig17'></a>
> Figure 17 - Application Android : Conception - Vue logique de haut niveau

### Vue logique de bas niveau<a name='logb'></a> [↑](#toc)

<img src="https://air.imag.fr/images/f/f5/VueLogiqueBasNiveau.png" width="80%"><a name='fig18'></a>
> Figure 18 - Application Android : Conception - Vue logique de bas niveau

### Vue physique<a name='phys'></a> [↑](#toc)

<img src="https://air.imag.fr/images/f/f5/PROJET-INFO5_1920_Secours_Montagne_avec_LoRa_VuePhysique.png" width="80%"><a name='fig19'></a>
> Figure 19 - Application Android : Conception - Vue physique

### Vues dynamiques<a name='dynq'></a> [↑](#toc)

<img src="https://air.imag.fr/images/3/3f/4-VueDynamique_RechercheVictime.png" width="80%"><a name='fig20'></a>
> Figure 20 - Application Android : Conception - Vue dynamique

### Arbres des tâches<a name='tree'></a> [↑](#toc)

<img src="https://air.imag.fr/images/2/2f/Modele-de-tache-PGHM.png" width="80%"><a name='fig21'></a>
> Figure 21 - Application Android : Conception - Modèle de tâches, Recherche d'une victime

### IHM abstraite<a name='ihma'></a> [↑](#toc)

<img src="https://air.imag.fr/images/1/13/IHM-abstraite.png" width="80%"><a name='fig22'></a>
> Figure 22 - Application Android : Conception - IHM Abstraite

### IHM concrète<a name='ihmc'></a> [↑](#toc)

<img src="https://air.imag.fr/images/e/eb/PGHM_IHM-concrete.png" width="80%"><a name='fig23'></a>  
> Figure 23 - Application Android : Conception - IHM Concrète (maquette)

La maquette de notre application a été réalisée sur le service Adobe Xd. Elle est disponible [ici](https://xd.adobe.com/view/b3f61cea-1d0e-42ca-5961-a546626d9a63-c5c7/?fbclid=IwAR0WLiHdUz783H2ZnUp4YPkFCrhJ1liNqZVVQekabv249N2p-BHwWKNfI2M) en version interactive.


## Conception du prototype de boitier<a name='proto'></a> [↑](#toc)
Nous tenions à disposer d’un prototype physique, afin de mieux imaginer ce que serait le produit St.Bernard s’il était commercialisé. Il était nécessaire que le boitier soit assez grand pour contenir la carte SODAQ Explorer, un module Bluetooth supplémentaire et les câbles entre les deux. Cependant, le boitier devait également être le plus compact et léger possible, pour se loger aisément et sans aucune contrainte dans un sac ou une poche de veste de ski.

Nous avons modélisé le boitier en 3D sur le logiciel Blender. Des trous ont été prévus pour pouvoir fixer la carte à l’intérieur du boitier, afin qu’elle ne bouge plus une fois installée. Une ouverture sur la façade permet également de connecter la carte en USB afin de l’alimenter électriquement, ou de recharger la pile incluse. Enfin, les rainures sur la face supérieure permettent d’y faire glisser un couvercle amovible.

<img src="https://air.imag.fr/images/f/f5/ConceptionBoitierStBernard.png" width="60%"><a name='fig24'>
> Figure 24 - Prototype du boitier : Conception en 3D

Nous avons ensuite donné vie à notre boitier grâce aux imprimantes 3D disponibles au FabLab MSTIC de l’UGA. Ce prototype n’est donc pas étanche. Pour le produit final, le boitier devrait être résistant aux chocs et à l’humidité afin d’être adapté aux conditions du milieu pour lequel il est conçu.

<img src="https://air.imag.fr/images/c/c1/ImpressionBoitierStBernard.png" width="80%"><a name='fig25'></a>
> Figure 25 - Prototype du boitier : Impression 3D

<img src="https://air.imag.fr/images/a/ab/BoitierStBernard.png"><a name='fig26'></a>
> Figure 26 - Prototype du boitier

## Code des cartes SODAQ ExpLoRer pour les mesures en montagne<a name='code'></a> [↑](#toc)
### Code de la carte émettrice<a name='codee'></a> [↑](#toc)
Un exemple de code de la carte émettrice utilisé pour les mesures expérimentales est disponible [ici](https://gitlab.com/info5_2020_secoursenmontagne/arduino/-/blob/test_P2P_SodaqIntern/Codes_Sortie_Montagne/TX_1_MIN/TX_1_MIN.ino).

### Code de la carte réceptrice<a name='coder'></a> [↑](#toc)
Un exemple de code de la carte réceptrice utilisé pour les mesures expérimentales est disponible [ici](https://gitlab.com/info5_2020_secoursenmontagne/arduino/-/blob/test_P2P_SodaqIntern/Codes_Sortie_Montagne/RX_1_MIN/RX_1_MIN.ino).

### Code final de la carte St.Bernard<a name='codef'></a> [↑](#toc)
Le code final est disponible [ici](https://gitlab.com/info5_2020_secoursenmontagne/arduino/-/blob/master/code_bernard/code_bernard.ino).

## Table des figures<a name='tof'></a> [↑](#toc)
[Figure 1](#fig1&2) -  Pré-étude : Emplacement géographique des mesures  
[Figure 2](#fig1&2) - Pré-étude : Affichage des mesures du SNR sur smartphone via Bluetooth   
[Figure 3](#fig3) - Pré-étude : Représentation graphique des valeurs du SNR en fonction de la distance  
[Figure 4](#fig4) - Pré-étude : Représentation graphique de la moyenne des SNR en fonction de la distance  
[Figure 5](#fig5) - Mesures en montagne : Schéma du protocole expérimental  
[Figure 6](#fig6) - Mesures en montagne : Affichage du SNR sur smartphone via Bluetooth  
[Figure 7](#fig7) - Mesures en montagne : Emplacement géographique  
[Figure 8](#fig8) - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission 14  
[Figure 9](#fig9) - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission -2  
[Figure 10](#fig10) - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission 14 (zoom)  
[Figure 11](#fig11) - Mesures en montagne : Moyenne du SNR selon la distance, puissance d'émission -2 (zoom)  
[Figure 12](#fig12) - Mesures en montagne : Moyenne du SNR selon la distance, prof 40 cm, puiss -2 (zoom)  
[Figure 13](#fig13) - Mesures en montagne : Données GPS  
[Figure 14](#fig14) - Mesures en montagne : Relevés GPS sur carte  
[Figure 15](#fig15) - Mesures en montagne : Relevés GPS sur carte + points à 0 mètres  
[Figure 16](#fig16) - Application Android : Conception - Diagramme de contexte  
[Figure 17](#fig17) - Application Android : Conception - Vue logique de haut niveau  
[Figure 18](#fig18) - Application Android : Conception - Vue logique de bas niveau  
[Figure 19](#fig19) - Application Android : Conception - Vue physique  
[Figure 20](#fig20) - Application Android : Conception - Vue dynamique  
[Figure 21](#fig21) - Application Android : Conception - Modèle de tâches, Recherche d'une victime  
[Figure 22](#fig22) - Application Android : Conception - IHM Abstraite  
[Figure 23](#fig23) - Application Android : Conception - IHM Concrète (maquette)  
[Figure 24](#fig24) - Prototype du boitier : Conception en 3D  
[Figure 25](#fig25) - Prototype du boitier : Impression 3D  
[Figure 26](#fig26) - Prototype du boitier  


---
---
## Récapitulatif [↑](#toc)

### Résumé 
Plus de 15000 demandes de secours en montagne sont recensées chaque année en France, soit près d'une quarantaine chaque jour. Si certaines situations sont sans grande gravité, d'autres relèvent de l'urgence absolue. La recherche d'une personne victime d'une avalanche et ensevelie sous la neige en est une, quand on sait qu'au-delà de 35 minutes, les chances de survie tombent à seulement 34%.  
Aujourd’hui, ce sont les DVA qui sont utilisés par les secours. Ils fonctionnent sur le principe du radar : la victime dispose d’un boîtier qui émet un signal dont la puissance oriente les secours. Mais celui-ci ne contient rien. S’il transmettait les coordonnées GPS de la victime, il deviendrait possible de s’en rapprocher immédiatement à quelques mètres seulement !  
Il existe donc un besoin de moderniser les technologies utilisées. Le but de ce projet est donc d’étudier la possibilité d'utiliser LoRa, alliée au GPS et au Bluetooth, pour retrouver plus facilement et rapidement une personne ensevelie.  
Nous étudierons le comportement des ondes LoRa dans la neige, afin de vérifier que leur utilisation est un bon choix dans ce contexte. Pour se faire, il sera nécessaire de définir un protocole expérimental de mesures en montagne afin de comprendre comment évolue la qualité des transmissions dans ces situations inhabituelles. Puis, nous développerons le système en lui-même. Il y aura une partie hardware (carte LoRa, capteur...) et une partie software (application pour smartphone).

### Summary
More than 15,000 distress calls, nearly forty a day, are recorded each year in France in the mountains. While some situations are not that serious, others represent an absolute emergency, for instance looking for an avalanche victim buried in the snow. Indeed, the chances of survival in such situation drop to only 34% beyond 35 minutes.  
Avalanche transceivers are the tools used today by emergency services. They roughly work like a radar: the victim has a box emitting a signal whose power directs the rescue team. However, such signal is empty of data. If it could send the victim's GPS coordinates, it would be possible for the rescue team to get to a more precise location quicker.  
There is therefore a need to modernize the technologies used. The aim of this project is to study the possibility of using the LoRa technology, combined with GPS and Bluetooth, to improve the searching of a snow-buried person.  
We will study the behaviour of LoRa waves in snow, in order to confirm the choice of such technology in this context. It will be necessary to define an experimental protocol to be applied in the field in order to understand how the quality of transmissions evolves in these unusual situations. We will then focus on the system itself. There will be a hardware part (LoRa card, sensor...) and a software part (smartphone application).
