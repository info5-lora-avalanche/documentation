# Cette branche récupère la documentation nécessaire à la compréhension de la partie Application Mobile du projet Secours en Montagne avec LoRa.

## Rapport
### Tome Principal
- [Version Markdown](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/%20Rapport_-_Tome_principal.md)
- [Version PDF](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/Rapport_-_Tome_principal.pdf)

### Annexes
Cela correspond également à ce qui a été nommé **Rapport technique** sur le Wiki Air.
- [Version Markdown](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/%20Rapport_-_Annexes.md)
- [Version PDF](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/Rapport_-_Annexes.pdf)

## Soutenances
- [Soutenance de mi-parcours (PDF)](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/Soutenance_mi-parcours.pdf)
- [Soutenance finale (PDF)](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/Soutenance_finale.pdf)

## Démonstration
- [Version PDF](https://gitlab.com/info5_2020_secoursenmontagne/application-mobile/-/blob/doc/D%C3%A9monstration.pdf)

## Wiki Air
- [Présentation du projet](https://air.imag.fr/index.php/Secours_Montagne_avec_LoRa)
- [Fiche de suivi](https://air.imag.fr/index.php/PROJET-INFO5_1920_Secours_Montagne_avec_LoRa)