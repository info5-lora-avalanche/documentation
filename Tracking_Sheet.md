## Agenda
### [Week 1 : 08/02 - 14/02](#w1)
### [Week 2 : 22/02 - 28/02](#w2)
### [26/02 : Mid-term presentation](#midterm)
### [Week 3 : 01/03 - 07/03](#w3)
### [Week 4 : 08/03 - 14/03](#w4)
### [Week 5 : 15/03 - 19/03](#w5)
### [19/03 : Final presentation](#final)

## Tracking Sheet

### Week 1 : 08/02 - 14/02 <a name="w1"></a>

#### 10/02

* We started to read all documents to understand the choosen solution:
    * The protocol which was designed for the project
    * The network architecture
    * The POC

#### 11/02

* We started the study of the existing code  

#### 12/02

* We continued the study of the existing code

### Week 2 : 22/02 - 28/02 <a name="w2"></a>
* Preparation for mid-term presentation
* We started the energy study
* Discussion with Olivier FABRE
* Changing the name of the project
* Schedule finalization
* Learning about React Native
* Initialization of the "Application Mobile" project

#### Mid-term presentation<a name="midterm"></a>
Remarks :
* Testing Leaflet with React Native
* We need to work faster = we haven't advanced enough

### Week 3 : 01/03 - 07/03 <a name="w3"></a>

#### 01/03

##### App 
* React native installation
* Initialization of React project

##### Chip
* Test the connection between a smartphone and a sodaq explorer with the bluetooth
* Research for the energy survey (ToA formula found)

#### 02/03 - 03/03

##### Chip
* Refactoring of the code
* Bluetooth module configuration + we can detect with the hardware that the module is connected

##### App 
* Splash screen done + icons
* Initialization of react native navigation
* Start working on the menu

##### Other
* Working on the poster for the presentation in english

#### 04/03 - 05/03

##### Chip

* Refactoring two functions:
    * string to hex
    * hex to string
* Now as we can detect if the bluetooth module is connected we wait that the bluetooth module is connected to continue the chip setup.

##### App 
* QR Code Scanner working
* Tests about menu with react navigation

#### 06/03

##### App 
* Tests about menu with react navigation
* Reoganisation of the code
* Menu for the app done

### Week 4 : 08/03 - 14/03 <a name="w4"></a>

### 08/03

##### App 
* Reworking some design aspect of the app
* Working on the ORM/DB

##### Chip
* Reflexion about package format of St-bernard (LoRa package)

### 09/03 - 10/03

##### App 
* Fixing and finishing ORM integration
* Enable modification of the informations

##### Chip
* Reflexion about operation modes of the chip

### 11/03

##### App
* Editing/Creation screen for new device

##### Chip
* Encoder of St-bernard package
* Decoder of St-bernard package
* Integration of encoder/decoder for St-bernard packages

### 12/03 - 14/03
##### App
* Reorganisation of the code
* Still working on the map
* Add a screen for our chip
* Add screens for the presentation of the application

##### Chip
* Working on BLE
* Working on how convert float into string and string into float
* Debug some blocks of code

### Week 5 : 15/03 - 19/03 <a name="w5"></a>
### 15/03 - 17/03

##### App
* Working on map : problems with old model 
* Working on bluetooth + eject project
* Start working on background fetch

#### Chip
* Debug encoder/decoder
* Debug BLE communication
* Debug LoRa communication
* Working on the workflow:
    * Test integration
    * Test with the both communication turned on
    * Debug the workflow 

+ working on the documentation of our code and documentation for final presentation
#### Final presentation <a name="final"></a>
