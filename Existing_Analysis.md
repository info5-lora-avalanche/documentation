# Analyse du projet existant

## CONSTAT

### Partie Arduino :

* Code à revoir (code dupliqué et éparpillé dans les branches)
* Problèmes au niveau du temps d'émission : ne respecte pas les normes ISM

### Partie Application mobile :

* Branches éparpillées (difficulté de savoir quelle branche héberge le code le plus récent)
* Code désastreux : gestion des erreurs douteuse, tests inexistants, complexité du code (beaucoup de while imbriqués)
* L'utilisation d'Android Studio pour le développement pose des problèmes pour certaines fonctionnalités à cause des différentes versions d'Android

### Documentation + Git

* Absence de documentation sérieuse, manque de documentation technique (difficile pour la reprise du code)
* Organisation du git désastreuse : présence de plusieurs branches avec aucune branche dédiée au développement et la branche master vide
* Aucun historique de version (tags, release)

### Divers

* POC "fonctionnel" d'après le rapport de fin de projet
* Modèle de trames créé exprès pour la transmission des données

## NOS OBJECTIFS

### Git + documentation

* Réorganisation du Git avec des règles claires de contribution, de commit et de création des branches
* Branches avec un système dev-master (cf [CONTRIBUTING.MD](https://gitlab.com/info5-lora-avalanche/application-mobile/-/blob/master/CONTRIBUTING.md)) pour assurer des livrables fonctionnels en continu

### Partie Arduino

* Réécriture du code de la carte SODAQ avec un code refactorisé
* Temps d'émission à retravailler : voir s'il est possible de le réduire
* Réaliser une estimation énergétique de la carte

### Partie Application mobile :

Amélioration et réécriture de cette partie

* refaire l'IHM de l'application
* repartir d'une base saine

Utilisations des technologies suivantes :

* Expo
* React Native
* Leaflet + autres technos qu'utilise déjà le PGHM => Voir pour reprendre une partie du code de Thomas réalisé durant son stage au PGHM
